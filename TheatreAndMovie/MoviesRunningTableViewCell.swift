//
//  MoviesRunningTableViewCell.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 12/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class MoviesRunningTableViewCell: UITableViewCell {

    @IBOutlet weak var nightShowOutlet: UIButton!
    @IBOutlet weak var matineeShowOutlet: UIButton!
    @IBOutlet weak var morningShowOutlet: UIButton!
    @IBOutlet weak var eveningShowOutlet: UIButton!
    @IBOutlet weak var theatreNameLabel: UILabel!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var distanceCalcLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       morningShowOutlet.layer.borderWidth = 0.5
        morningShowOutlet.layer.cornerRadius = 16
        morningShowOutlet.layer.borderColor = UIColor.lightGray.cgColor
        matineeShowOutlet.layer.borderColor = UIColor.lightGray.cgColor
        matineeShowOutlet.layer.cornerRadius = 16
       matineeShowOutlet.layer.borderWidth = 0.5
       eveningShowOutlet.layer.borderWidth = 0.5
        eveningShowOutlet.layer.borderColor = UIColor.lightGray.cgColor
        eveningShowOutlet.layer.cornerRadius = 16
        nightShowOutlet.layer.cornerRadius = 16
        nightShowOutlet.layer.borderColor = UIColor.lightGray.cgColor
       nightShowOutlet.layer.borderWidth = 0.5
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
