//
//  GoogleSDK.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 08/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import GoogleSignIn

class GoogleSDK: NSObject,GIDSignInDelegate,GIDSignInUIDelegate {
      static let shared = GoogleSDK()
    //MARK: Internal Properties
    var signInBlock: ((GIDGoogleUser) -> Void)?
    
    func googleSignIn()  {
        GIDSignIn.sharedInstance().delegate = self
//        GIDSignIn.sharedInstance().signOut()
//        GIDSignIn.sharedInstance().signIn()
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.login")
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.me")
        if GIDSignIn.sharedInstance().hasAuthInKeychain() == true {
            GIDSignIn.sharedInstance().signInSilently()
        } else {
            GIDSignIn.sharedInstance().signIn()
        }
    }
    
    //Google SignIn Delegates
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil {
             self.signInBlock?(user)
            
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
      //  signInBlock(nil)
//        if signInBlock != nil {
//            signInBlock!(false, nil, error)
//        }
    }
    
}
