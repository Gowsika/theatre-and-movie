//
//  FacebookSDK.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 08/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FacebookLogin

class FacebookSDK: NSObject {
    static let shared = FacebookSDK()
    var getUserData: (([String: Any]) -> Void)?
    
    func FaceBook(vcName: UIViewController) {
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [.publicProfile, .email], viewController: vcName, completion: { loginResult in
            switch loginResult {
            case .failed(let error):
                self.getUserData!([:])
                print(error)
            case .cancelled:
                self.getUserData!([:])
                print("User cancelled login.")
            case .success(_,  _, _):
                self.getFBUserData()
            }
        })
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email, birthday,gender"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    print(result!)
                    self.getUserData?(result as! [String : Any])
                }
            })
        }
    }
}
