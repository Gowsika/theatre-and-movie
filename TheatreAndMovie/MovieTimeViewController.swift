//
//  MovieTimeViewController.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 11/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class MovieTimeViewController: UIViewController, UITableViewDelegate,UITableViewDataSource   {
    var theatreList = ["Vox Cinemas", "Novo Cinemas", "Grand Al Mariah Center", "Cine Royal Ruwais Mall"]
    var cityDatalist = ["Abu Dhabi","Abu Dhabi", "Abu Dhabi", "Abu Dhabi"]
    var distanceDataList = ["4 KM", "6 KM", "20 KM", "3 KM"]
  
   @IBOutlet weak var moviesRunTable: UITableView!
  override func viewDidLoad() {
        super.viewDidLoad()
        
//        let calendar = FSCalendar(frame: CGRect(x: 0, y: 0, width: 320, height: 170))
//        calendar.dataSource = self
//        calendar.delegate = self
//        calendarView.addSubview(calendar)
//       self.calendar = calendar
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
      
        moviesRunTable.delegate = self
        moviesRunTable.dataSource = self
        let navigationBar = navigationController?.navigationBar
        navigationBar?.barTintColor = UIColor(red: 218/255, green: 60/255, blue: 49/255, alpha: 1)
        
        navigationBar?.isTranslucent = false
        navigationBar?.tintColor = UIColor.white
        let rightBarButton = UIButton.init(type: .custom)
        rightBarButton.setImage(UIImage.init(named: "filter_icon"), for: .normal)
        rightBarButton.frame = CGRect(x: 280, y: 20, width: 30, height: 30)
        let rightBar = UIBarButtonItem.init(customView: rightBarButton)
        self.navigationItem.rightBarButtonItem = rightBar
      
       }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return theatreList.count
    }
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MoviesRunningTableViewCell = moviesRunTable.dequeueReusableCell(withIdentifier: "MoviesCell") as! MoviesRunningTableViewCell
        cell.theatreNameLabel.text = self.theatreList[indexPath.row]
        cell.cityNameLabel.text = self.cityDatalist[indexPath.row]
        cell.distanceCalcLabel.text = self.distanceDataList[indexPath.row]
        cell.accessoryType = .detailButton
        cell.tintColor = UIColor.lightGray
        cell.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        let bgView: UIView = UIView(frame: CGRect(x: 5, y: 5, width: moviesRunTable.frame.size.width - 8, height: cell.contentView.frame.size.height - 5))
        bgView.isUserInteractionEnabled = true
        bgView.layer.backgroundColor = UIColor.white.cgColor
        bgView.layer.borderWidth = 0.5
        cell.contentView.addSubview(bgView)
        cell.contentView.sendSubview(toBack: bgView)
        return cell
    }


}
