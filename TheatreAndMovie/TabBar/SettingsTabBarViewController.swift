//
//  SettingsTabBarViewController.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 08/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
import FacebookLogin

class SettingsTabBarViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var items = [["Edit Profile", "App Settings","Notifications"],["About", "Rate our app" , "Privacy Policy","Help & Feedback"],["Logout"]]
    var images =  [["user_icon1","app_setting_icon","notifcation"],["about_icon","rate_icon","privacy_icon","help_icon"],["logout_icon"]]
  
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var settingsTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
       self.tabBarController?.delegate = self
        self.view.tag = 999
        self.tabBarController?.delegate = self
        settingsTableView.delegate = self
        settingsTableView.dataSource = self
        profilePic.layer.cornerRadius = profilePic.frame.size.width / 2
        profilePic.clipsToBounds = true
        profilePic.layer.borderColor = UIColor.black.cgColor
        }
   override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
       }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = settingsTableView.dequeueReusableCell(withIdentifier: "SettingsCell", for: indexPath) as! CellSettingsTableViewCell
        cell.textLabel?.text = items[indexPath.section][indexPath.row]
        cell.textLabel?.font = UIFont(name: "SF Compact Display", size: 18)
      //  cell.layer.borderWidth = 0.5
        cell.imageView?.image = UIImage(named: images[indexPath.section][indexPath.row])
        cell.accessoryType = .disclosureIndicator
      if indexPath.section == 2{
      cell.accessoryType = .none
    }
    return cell
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
   func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 2
    }
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selected = self.items[indexPath.section][indexPath.row]
        if (selected.isEqual("Logout")){
            // removed login details
            TAMUser.currentUser = nil
            TAMUser.removeCurrentUser()
            GIDSignIn.sharedInstance().signOut()
            LoginManager().logOut()
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.changeLoginVC()
        }else if (selected.isEqual("App Settings")){
            //  var preferredLanguage : String = Bundle.main.preferredLocalizations.first!
            let AppVC: AppSettingsViewController = UIStoryboard(storyboard: .main).instantiateViewController()
            self.appUtilityPush(AppVC)
//            if let pre = Bundle.main.preferredLocalizations.first, !pre.isEmpty  {
//                if pre == Language.english.rawValue{
//                    Language.language = Language.arabic
//                }else{
//                    Language.language = Language.english
//                }
//                             }

        }else if(selected.isEqual("Notifications")){
            let NotifyVC: NotificationViewController = UIStoryboard(storyboard: .main).instantiateViewController()
            self.appUtilityPush(NotifyVC)
        }else if(selected.isEqual("Edit Profile")){
            let editVC: EditProViewController = UIStoryboard(storyboard: .main).instantiateViewController()
            self.appUtilityPush(editVC)
        }
                   }
 

}
