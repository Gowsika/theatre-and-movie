//
//  TheatreTabBarViewController.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 08/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
 class TheatreTabBarViewController: UIViewController, UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,MKMapViewDelegate, CLLocationManagerDelegate {
    var showActive: Bool = false
    var locationManager: CLLocationManager!
    var getTheatreName = [[String: Any]]()
    var filtered = [[String: Any]]()
    var getPreviousData = [String: Any]()
    let searchBar = UISearchBar(frame: .zero)
    @IBOutlet weak var distanceTableView: UITableView!
    @IBOutlet weak var theatreMapView: MKMapView!
    var setNoDataResults = ""
    //MARK: - Load Methods
    override func viewDidLoad() {
        distanceTableView.removeEmptyCell()
      //  self.callMapView()
        super.viewDidLoad()
        self.tabBarController?.delegate = self
         callMapView()
        distanceTableView.showsVerticalScrollIndicator = false
        distanceTableView.delegate = self
        distanceTableView.dataSource = self
             }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
             }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !getTheatreName.isEmpty{
            getTheatreName.removeAll()
            self.getTheatre()
            }
                }
    //MARK: - Class Met
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         searchBar.text = ""
        setNoDataResults = ""
        showActive = false
         filtered.removeAll()
        hideSearchBar()
             }
    @objc func hideKeyboard(){
        view.endEditing(true)
    }
        func callMapView(){
      locationManager = CLLocationManager()
         locationManager.delegate = self
      //  locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
      
       //  locationManager.requestLocation()
       // theatreMapView.delegate = self
        locationManager(locationManager, didChangeAuthorization: CLLocationManager.authorizationStatus())
        // locationManager(manager: locationManager, status: CLLocationManager.authorizationStatus())
    //
               }
    func showMapview(){
        theatreMapView.showsUserLocation = true
        theatreMapView.delegate = self
        getTheatreName.removeAll()
        self.getTheatre()
        }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
          //  manager.requestLocation()
            break
        case .authorizedWhenInUse:
             //   SHOWAlert.setinfo(title: "Comes here authorizedWhenInUse ", subTitle:"")
           self.showMapview()
            break
        case .restricted:
            SHOWAlert.setinfo(title: "", subTitle: "Enable Location Services")
            // restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            SHOWAlert.setinfo(title: "Allow Location", subTitle: "Enable Your Location access from Settings")
                // user denied your app access to Location Services, but can grant access from Settings.app
            break
        case .authorizedAlways:
       // SHOWAlert.setinfo(title: "Comes here authorizedAlways ", subTitle:"")
               self.showMapview()
                break
        }
    }
    
    func sethideState(status: Bool){
      //  theatreMapView.isHidden = status
        searchBar.isHidden = status
    }
    
    func getTheatre(){
        showSpinner()
        NetworkAdapter.methodPOST(.getTheatreList(), successCallback: { (response) in
            do{
                let getJSON: Any = try JSONSerialization.jsonObject(with: response.data, options: [])
               print(getJSON)
                let resultsArr = getJSON as! [String: Any]
                let data = resultsArr["Results"] as! [[String: Any]]
                if resultsArr["Status"] as? String == "Failed"{
                    
                    self.getTheatreName.removeAll()
                    self.sethideState(status: true)
                      DispatchQueue.main.async {
                        self.setNoDataResults = "No theatre results found"
                     self.distanceTableView.reloadData()
                    }
//                    let datum = resultsArr["Results"] as! [Any]
//                    SHOWAlert.setinfo(title: "Results Empty..!", subTitle: (datum[0] as? String)!)
//                    return
                }else
                if  !data.isEmpty{
                     self.sethideState(status: false)
                    if !self.getTheatreName.isEmpty{self.getTheatreName.removeAll()}
                    self.getTheatreName.append(contentsOf: data)
                   //.coordinate.latitude
                    DispatchQueue.main.async {
                         self.showAnnotation(mapArray: self.getTheatreName)
//                        let location: CLLocationCoordinate2D  =   CLLocationCoordinate2D(latitude: , longitude: (self.theatreMapView.userLocation.location?.coordinate.longitude)!)
//                           print(location.latitude)
//                         print(location.longitude)
                    }
                   
                }
                
            }catch{
                self.distanceTableView.EmptyMessage(message: "No Theatres Found")
                //   print("getJSON")
            }
        }, error: { (error) in
            print("error", error.localizedDescription)
        }, failure: {(error) in
            print("error", error.localizedDescription)
        })
    }
    
    func mapView(_ mapView: MKMapView, didFailToLocateUserWithError error: Error) {
        print(error.localizedDescription)
    }
    func showAnnotation(mapArray: [[String: Any]]){
        theatreMapView.removeAnnotations(theatreMapView.annotations)
        for data in mapArray{
            let point = MKPointAnnotation()
            point.title = data["Theatrename"] as? String
            point.subtitle = data["Address"] as? String
            let location: CLLocationCoordinate2D  = CLLocationCoordinate2DMake(Double(data["Latitude"] as! String)! ,Double(data["Longitude"] as! String)!)
            //let location: CLLocationCoordinate2D  =   CLLocationCoordinate2D(latitude: 9.939093, longitude: 78.121719)
            point.coordinate = location
         //   print(point.coordinate)
            theatreMapView.addAnnotation(point)
            var region = theatreMapView.region
            region.center = location
            region.span.longitudeDelta /= 0.5
            region.span.latitudeDelta /= 0.5
            theatreMapView.region = region
        }
        DispatchQueue.main.async {
       self.distanceTableView.reloadData()
        }
    }
    //MARK: - Action Method
    @IBAction func handleSearch(_ sender: UIBarButtonItem) {
        if !getTheatreName.isEmpty   {
            
            searchBar.placeholder = "Search Theatre names"
            searchBar.showsCancelButton = true
            searchBar.enablesReturnKeyAutomatically = true
            searchBar.delegate = self
            searchBar.tintColor = .white
            navigationItem.titleView = searchBar
    }
    }
    //MARK: - TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return showActive ? !filtered.isEmpty ? filtered.count : 0 : getTheatreName.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if getTheatreName.isEmpty && !showActive{
        tableView.EmptyMessage(message:  setNoDataResults)
        }else if filtered.isEmpty && showActive{
            tableView.EmptyMessage(message:  setNoDataResults)
        }
        else{
            tableView.restore()
        }
        return  showActive ? !filtered.isEmpty ? 1 : 0 : !getTheatreName.isEmpty ? 1 : 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DistanceCell") as! CellTheatreTableViewCell
//        let bgView: UIView = UIView(frame: CGRect(x: 0, y: 5, width: tableView.frame.size.width, height: cell.contentView.frame.size.height - 10))
//        bgView.isUserInteractionEnabled = true
//        bgView.layer.backgroundColor = UIColor.white.cgColor
//        bgView.layer.borderWidth = 0.5
//        cell.contentView.addSubview(bgView)
//        cell.contentView.sendSubview(toBack: bgView)
        var getSubObj = [String: Any]()
        if !getTheatreName.isEmpty && !showActive{
            getSubObj = getTheatreName[indexPath.row]
        }else if !filtered.isEmpty && showActive{
            getSubObj = filtered[indexPath.row]
        }
        if getSubObj.count > 0{
            convertdistanceToKM(getSubObj: getSubObj, cell: cell)
        }
        cell.accessoryType = .detailButton
        cell.tintColor = .lightGray
      //  cell.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        return cell
    }
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        print("VALID")
    }
 
    @objc func getUserLocation () -> CLLocation{
//        if self.theatreMapView.isUserLocationVisible{
//             print("nil")
//        }
        if let getCoordinate  = self.theatreMapView.userLocation.location?.coordinate{
          return CLLocation(latitude: (getCoordinate.latitude) , longitude: (getCoordinate.longitude))
        //   return CLLocation
            
        }
           return CLLocation(latitude: (0.0) , longitude: (0.0))
       // print("nil")
    //   return CLLocation(latitude: (.latitude) , longitude: (self.theatreMapView.userLocation.location?.coordinate.longitude)!)

    }
         func convertdistanceToKM(getSubObj: [String: Any],cell: CellTheatreTableViewCell){
      //  let userCurrLoc: CLLocation = locationManager.location!
        let theatreLocation: CLLocation =  CLLocation(latitude: Double(getSubObj["Latitude"] as! String)!, longitude: Double(getSubObj["Longitude"] as! String)!)
           
      let distanceinKM: CLLocationDistance = getUserLocation().distance(from: theatreLocation).inKilometers()
          //  print(String(format:"%d KM",Int(distanceinKM)))
           // print( cell.distanceLabel)
         cell.distanceLabel.text =  String(format:"%d KM",Int(distanceinKM))
            cell.textLabel?.text = getSubObj["Theatrename"] as? String ?? ""
        
        if let getCity = getSubObj["citylist"] as? [String: Any], let getState = getSubObj["state"] as? [String: Any], let getCountry = getSubObj["country"] as? [String: Any] {
            cell.detailTextLabel?.text = String(format:"%@,%@,%@",(getCity["Cityname"] as? String)!,(getState["Statename"] as? String)!,
            (getCountry["Countryname"] as? String)!)
        }
    }
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   // let cell = tableView.cellForRow(at: indexPath)
     tableView.deselectRow(at: indexPath, animated: true)
  
    let newVc: TheatreDetailsViewController = UIStoryboard(storyboard: .main).instantiateViewController()
    var getSubObj = [String: Any]()
    if !getTheatreName.isEmpty && !showActive{
        getSubObj = getTheatreName[indexPath.row]
    }else if !filtered.isEmpty && showActive{
        getSubObj = filtered[indexPath.row]
    }
   // print(getSubObj)
    newVc.navTitle =  getSubObj["Theatrename"] as? String ?? ""
    newVc.getprevTheatreDetails = getSubObj
    self.appUtilityPush(newVc)
    }
          //MARK: - searchBar Delegate Methods
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if searchText.isEmpty{
            searchBar.resignFirstResponder()
        }
        filtered = getTheatreName.filter{ (($0["Theatrename"] as? String)?.containsIgnoringCase(find: searchText))! }
        showActive = !filtered.isEmpty
         if filtered.isEmpty   {
            if !searchText.isEmpty{
                showActive = true
                setNoDataResults = "No theatre results found"
            }else{
                showActive = false
                setNoDataResults = ""
            }
            distanceTableView.reloadData()
        }else{
            if showActive{
                self.showAnnotation(mapArray: filtered)
                
            }else{
                self.showAnnotation(mapArray: getTheatreName)
            }
        }
                  }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        //distanceTableView.reloadData()
    }
    func showCancelButton(Status: Bool){
        searchBar.showsCancelButton = Status
    }
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool{
      showCancelButton(Status: true)
        return true
    }
    func hideSearchBar(){
        searchBar.resignFirstResponder()
        searchBar.removeFromSuperview()
        showTitle()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        distanceTableView.reloadData()
        hideSearchBar()
    }
    func showTitle(){
        let titleView = UILabel(frame: CGRect(x: 50, y: 0, width: 220, height: 44))
        titleView.text = "Theatres"
        titleView.textColor = .white
        titleView.textAlignment = .center
        navigationItem.titleView = titleView
    }
func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    if annotation is MKUserLocation {
            return nil
        }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "pin") as? MKPinAnnotationView
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        } else {
            annotationView?.annotation = annotation
        }
        
        if let ann = annotationView {
            ann.pinTintColor = .red
            ann.canShowCallout = true
            ann.rightCalloutAccessoryView = UIButton(type: .infoLight)
            ann.animatesDrop = true
        }
                 return annotationView
    }
      }
extension TheatreTabBarViewController{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last{
            print(location)
//            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
//            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
//            self.theatreMapView.setRegion(region, animated: true)
              manager.stopUpdatingLocation()
            //manager.stop
          //   self.showMapview()
             if getTheatreName.isEmpty{
                self.showMapview()
//                self.showAnnotation(mapArray: getTheatreName)
   }
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error", error.localizedDescription)
    }
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let baseTabBarVC = segue.destination as? TheatreDetailsViewController   {
//            print(getPreviousData)
//            if let getID = getPreviousData["theatre_id"] as? Any {
//               // baseTabBarVC.getTheatreId = String(format:"%@", getID as! CVarArg)
//            }
//            else if let getID = getPreviousData["theatre_id"] as? Int {
//                baseTabBarVC.getTheatreId = getID
//             }
//                     }
//    }
}
