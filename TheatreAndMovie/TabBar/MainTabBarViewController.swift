//
//  MainTabBarViewController.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 08/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController {

    var setSelectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(setSelectedIndex)
        self.selectedIndex = setSelectedIndex
              }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    

}
