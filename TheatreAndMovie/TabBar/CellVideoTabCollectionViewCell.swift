//
//  CellVideoTabCollectionViewCell.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 29/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class CellVideoTabCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var censorLabel: UILabel!
    @IBOutlet weak var movieImage: UIImageView!
}
