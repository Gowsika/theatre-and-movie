//
//  FilterViewController.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 30/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import Foundation
  protocol filterDelegate: class {
    func passFilter(currntObj: [String: Any], reset: Bool)
}
 struct setfilterDetails {
    static var lang: String = ""
    static var sortBy: String = ""
}
 class FilterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
         var sortByData = ["Popularity", "ReleaseDate", "Alphabetically"]
    var sortByDataImg = ["popularity_icon","release_icon","alphabet_icon"]
    var setSelectedFilter = [String: Any]()
    var setLanguages = [[String: String]]()
    var storeFirstSec = [Int](repeating: -1, count: 1)
    var storeSecondSec = [Int](repeating: -1, count: 1)
    
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var filterTableView: UITableView!
    weak  var delegate: filterDelegate?
    
    //MARK: - Load Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setSelectedFilter["user_id"] = "0"
        getLanguages()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    //MARK: - Action Methods
    @IBAction func resetAction(_ sender: UIButton) {
        removeAllSelections(currentSec: 0)
        removeAllSelections(currentSec: 1)
        //    self.dismissPresentVC()
        //     dismissCurrentVC(passCurr: [:], reset: true)
        // cancelTableAction()
    }
    //    @objc func checkAction(){
    //        if (iconsClick == true) {
    //            button.setImage(UIImage(named: "checkbox"), for: .normal)
    //            iconsClick = false
    //        }else {
    //            button.setImage(UIImage(named: "CheckedBox"), for: .normal)
    //            iconsClick = true
    //        }
    //    }
    @IBAction func cancelFilterAction(_ sender: Any) {
        appUtilityPop()
        //dismissCurrentVC(passCurr: [:],reset: false)
    }
    func  dismissCurrentVC(passCurr: [String: Any],reset: Bool ){
        delegate?.passFilter(currntObj: passCurr, reset: reset )
        appUtilityPop()
    }
    //    @objc func cancelTableAction(){
    //        let nav = storyboard?.instantiateViewController(withIdentifier: "MainTabBarViewController") as! MainTabBarViewController
    //        self.present(nav, animated: true, completion: nil)
    //    }
    @IBAction func callFilterFunction(_ sender: Any) {
        
        if !setfilterDetails.sortBy.isEmpty ||  !setfilterDetails.lang.isEmpty{
            print(setSelectedFilter)
            setSelectedFilter["sortBy"] = setfilterDetails.sortBy
            setSelectedFilter["language"] = setfilterDetails.lang
            dismissCurrentVC(passCurr: setSelectedFilter,reset: false)
        }
    }
    
         func getLanguages(){
        
        NetworkAdapter.methodPOST(.getLanguages(), successCallback: {(response) in
            do {
                let applyJSON = try JSONSerialization.jsonObject(with: response.data, options: []) as! [String : Any]
               
                if let Retriever = applyJSON["Results"] as? [[String: Any]] {
                    //  print(Retriever)
                    if !Retriever.isEmpty{
                        
                        for getEachObj in Retriever {
                            for (key, value) in getEachObj {
                                // print("\(key): \(value)")
                                var temp = [String: String]()
                                temp["lang_key"] = key
                                temp["lang_name"]  = value as? String
                                self.setLanguages.append(temp)
                            }
                        }
                        self.filterTableView.reloadData()
                    }
                                     }
            }catch{
                print("applyJSON")
            }
        }, error: {(Error) in
            print("error", Error.localizedDescription)
        }, failure: {(Error) in
            print("error", Error.localizedDescription)
        })
    }
    
    //MARK: - TableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return    !setLanguages.isEmpty ? 2 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? sortByData.count :  setLanguages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = filterTableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath) as! FilterHeaderTableViewCell
        cell.accessoryType = .none
        cell.tintColor = .clear
        switch (indexPath.section) {
        case 0:
            cell.textLabel?.text = sortByData[indexPath.row]
            cell.imageView?.image = UIImage(named: self.sortByDataImg[indexPath.row])
           
            /*
            let rowNumber: Int = Int(indexPath.row)
            
            if storeFirstSec.contains(rowNumber) {
                // print("yes")
                cell.accessoryType = .checkmark
                cell.tintColor =  .red
                
            }else{
                cell.accessoryType = .none
                cell.tintColor = .clear
            } */
        case 1:
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 22))
            let getString = setLanguages[indexPath.row]["lang_name"]
            label.text = String((getString?.prefix(2))!)
            let image = UIImage.imageWithLabel(label)
            cell.imageView?.image = image
            cell.textLabel?.text = getString
        /*
            let rowNumber: Int = Int(indexPath.row)
            if storeSecondSec.contains(rowNumber) {
                // print("yes")
                cell.accessoryType = .checkmark
                cell.tintColor =  .red
            }else{
                cell.accessoryType = .none
                cell.tintColor = .clear
            }*/
            
        default:
            break
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let cell = tableView.cellForRow(at: indexPath)
        let rowNumber: Int = Int(indexPath.row)
        removeAllSelections(currentSec: indexPath.section)
        switch (indexPath.section) {
        case 0:
            setfilterDetails.sortBy = String(format:"%d",indexPath.row + 1)
            storeFirstSec.append(rowNumber)
            cell?.accessoryType = .checkmark
            cell?.tintColor =  .red
            /*
             if storeFirstSec.contains(rowNumber) {
             // print("yes")
             setfilterDetails.sortBy = ""
             //  cell?.accessoryType = .none
             //  cell?.tintColor = .clear
             }else{
             setfilterDetails.sortBy = String(format:"%d",indexPath.row + 1)
             storeFirstSec.append(rowNumber)
             cell?.accessoryType = .checkmark
             cell?.tintColor =  .red
             } */
        case 1:
            //    let rowNumber: Int = Int(indexPath.row)
            setfilterDetails.lang = setLanguages[indexPath.row]["lang_key"]!
            storeSecondSec.append(rowNumber)
            cell?.accessoryType = .checkmark
            cell?.tintColor =  .red
            /*
             if cell?.accessoryType == .none {
             cell?.accessoryType = .checkmark
             cell?.tintColor =  .red
             }else{
             cell?.accessoryType = .none
             cell?.tintColor = .clear
             }
             if storeSecondSec.contains(rowNumber) {
             // print("yes")
             setfilterDetails.lang = ""
             storeSecondSec.remove(at: rowNumber)
             }else{
             setfilterDetails.lang = setLanguages[indexPath.row]["lang_key"]!
             storeSecondSec.append(rowNumber)
             } */
        default:
            break
        }
    }
    
    func removeAllSelections(currentSec:Int){
        let totalRows = filterTableView.numberOfRows(inSection: currentSec)
        if currentSec == 0 {
            setfilterDetails.sortBy = ""
            storeFirstSec.removeAll()
        }else{
            storeSecondSec.removeAll()
            setfilterDetails.lang = ""
        }
        
        for rows in 0..<totalRows {
            let cell = filterTableView.cellForRow(at: IndexPath(row: rows, section: currentSec))
            cell?.accessoryType = .none
            cell?.tintColor = .clear
        }
    }
    /*
     func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
     let cell = tableView.cellForRow(at: indexPath)
     switch (indexPath.section) {
     case 0:
     setfilterDetails.sortBy = ""
     tableView.cellForRow(at: indexPath)?.accessoryType = .none
     case 1:
     setfilterDetails.lang = ""
     tableView.cellForRow(at: indexPath)?.accessoryType = .none
     default:
     break
     }
     
     cell?.tintColor = .clear
     
     }
     
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     tableView.deselectRow(at: indexPath, animated: true)
     let selected = self.sortByData[indexPath.row]
     let cell = tableView.cellForRow(at: indexPath)
     cell?.accessoryType = .none
     cell?.tintColor = .clear
     setSotBy = ""
     cell?.accessoryType = .checkmark
     cell?.tintColor =  .red
     setSotBy = selected
     //            if setSotBy.containsIgnoringCase(find: selected) && !setSotBy.isEmpty  {
     //                setSotBy = ""
     //            }
     //
     //       else{
     //            cell?.accessoryType = .checkmark
     //            cell?.tintColor =  .red
     //            setSotBy = selected
     //        }
     
     } */
    
    //        if selected.isEqual("Popularity"){
    //           applyFilter()
    //        }
    //       else if selected.isEqual("ReleaseDate"){
    //            applyFilter()
    //        }
    //        else if selected.isEqual("Alphabetically"){
    //            getAlphabetical()
    //        }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = filterTableView.dequeueReusableCell(withIdentifier: "HeaderCell") as! FilterHeaderTableViewCell
        header.backgroundColor = UIColor.lightGray.withAlphaComponent(0.3)
        
        switch (section) {
        case 0:
            header.textLabel?.text = "Sort by"
        case 1:            header.textLabel?.text = "Language"
        default:
            break
        }
        return header
    } /*
     func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
     guard section == 1
     else{
     return nil
     }
     let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.filterTableView.frame.width, height: 40.0))
     let cancelAction = UIButton(frame: CGRect(x: 0, y: 0, width: self.filterTableView.frame.width / 2, height: 40.0))
     cancelAction.setTitle("Cancel", for: .normal)
     cancelAction.setTitleColor(UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1), for: .normal)
     cancelAction.layer.borderWidth = 0.5
     cancelAction.layer.borderColor = UIColor.lightGray.cgColor
     cancelAction.addTarget(self, action: #selector(cancelTableAction), for: .touchUpInside)
     footerView.addSubview(cancelAction)
     let applyAction = UIButton(frame: CGRect(x: filterTableView.frame.midX, y: 0, width: self.filterTableView.frame.width / 2, height: 40.0))
     applyAction.setTitle("Apply Filter", for: .normal)
     applyAction.setTitleColor(UIColor(red: 216/255, green: 72/255, blue: 63/255, alpha: 1), for: .normal)
     applyAction.layer.borderWidth = 0.5
     applyAction.layer.borderColor = UIColor.lightGray.cgColor
     applyAction.addTarget(self, action: #selector(applyFilter), for: .touchUpInside)
     footerView.addSubview(applyAction)
     return footerView
     }
     func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
     return  section == 0 ? 0 : 40.0
     }*/
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
}
