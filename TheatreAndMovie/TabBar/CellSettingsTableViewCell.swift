//
//  CellSettingsTableViewCell.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 26/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class CellSettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var setLogoImage: UIImageView!
    @IBOutlet weak var settingsLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
