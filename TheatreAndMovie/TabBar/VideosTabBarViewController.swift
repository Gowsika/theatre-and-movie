 //
 //  VideosTabBarViewController.swift
 //  TheatreAndMovie
 //
 //  Created by Gowsika on 08/06/18.
 //  Copyright © 2018 com.dci. All rights reserved.
 //
 
 import UIKit
 import SDWebImage
 import SwiftSpinner
// import DOFavoriteButton
   class VideosTabBarViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, filterDelegate {
         var getId = [Int]()
    var iconsClick: Bool!
    var nowShowImage = [[String: Any]]()
    var nowImage = [String]()
    var nowShowLanguage = [[String: Any]]()
    var nowShowName = [[String: Any]]()
    var nowShowCertify = [[String: Any]]()
    var upComeName = [[String: Any]]()
    var getAllButtons = [UIButton]()
    var getAddData = [[String: Any]]()
    var getLikeValue = [String: Any]()
    var getUserId: String!
    @IBOutlet weak var scrollViewOutlet: UIScrollView!
    @IBOutlet weak var upcomingTable: UITableView!
    @IBOutlet weak var videosTableView: UITableView!
    @IBOutlet weak var nowShowingAction: UIButton!
    @IBOutlet var enableCorner: [UIButton]!
    @IBOutlet weak var comingSoonAction: UIButton!
    @IBOutlet weak var exclusiveOutlet: UIButton!
    // MARK: Stored Properties
    internal var getUserDetails: TAMUser!
    override func viewDidLoad() {
        // filterContainerView.isHidden = true
        videosTableView.removeEmptyCell()
        upcomingTable.removeEmptyCell()
        super.viewDidLoad()
        // user already looged in
        self.tabBarController?.delegate = self
        updateScrollsubviews()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func updateScrollsubviews(){
        // add all buttons to array
        getAllButtons.append(self.nowShowingAction)
        getAllButtons.append(self.comingSoonAction)
        getAllButtons.append(self.exclusiveOutlet)
        DispatchQueue.main.async {
            self.scrollViewOutlet.translatesAutoresizingMaskIntoConstraints = false
            var getScroll = self.scrollViewOutlet.frame
            getScroll.size.width = self.getDeviceWidth()
            self.scrollViewOutlet.frame = getScroll
            // self.upcomingTable.translatesAutoresizingMaskIntoConstraints = false
            var getupcomesTbl = self.upcomingTable.frame
            getupcomesTbl.size.width = self.getDeviceWidth() - 13
            getupcomesTbl.origin.x =   self.getDeviceWidth() + 7 //self.videosTableView.frame.origin.x +  self.videosTableView.frame.size.width + 5
            self.upcomingTable.frame = getupcomesTbl
            self.scrollViewOutlet.addSubview(self.upcomingTable)
            // self.videosTableView.translatesAutoresizingMaskIntoConstraints = false
            var getVideosTbl = self.videosTableView.frame
            getVideosTbl.size.width = self.getDeviceWidth() - 15
            self.videosTableView.frame = getVideosTbl
            self.scrollViewOutlet.addSubview(self.videosTableView)
            //            getScroll.size.width = self.getDeviceWidth() //* 2
            //            self.scrollViewOutlet.frame = getScroll
            var contentSize: CGSize = self.scrollViewOutlet.frame.size
            contentSize.width = CGFloat(2) * self.getDeviceWidth()
            self.scrollViewOutlet.contentSize = contentSize
            //self.scrollViewOutlet.contentSize = CGSize(width: self.getDeviceWidth() * 2, height:  self.scrollViewOutlet.frame.size.height)
            self.view.layoutIfNeeded()
            print(self.scrollViewOutlet.frame.origin.y)
            self.videosTableView.showsVerticalScrollIndicator = false
            self.nowShowingBtn(self.nowShowingAction)
        }
    }
    
    func updateCornerradious(sender: UIButton){
        for getBut in getAllButtons{
            getBut.backgroundColor = .clear
        }
        sender.layer.cornerRadius = 10
        sender.backgroundColor = UIColor.black.withAlphaComponent(0.2)
    }
    
    var lastContentOffset: CGFloat = 0.0
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.x
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //  scrollDirectionDetermined = false
        if self.lastContentOffset < scrollView.contentOffset.x {
            // moved right
            movebuttonsShadows(scrollView: scrollView)
        } else if self.lastContentOffset > scrollView.contentOffset.x {
            // moved left
            movebuttonsShadows(scrollView: scrollView)
        } else {
            // didn't move
        }
        //        let translation =  scrollView.panGestureRecognizer.translation(in: scrollView.superview)
        //        if translation.y > 0 || translation.y < 0 {
        //            return
        //        }else{
        //
        //        }
    }
    
    func movebuttonsShadows(scrollView: UIScrollView){
        let pageWidth: CGFloat = scrollView.frame.size.width
        let page: Int = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
        print(page)
        if page == 0{
            updateCornerradious(sender: self.nowShowingAction)
        }else if page == 1{
            updateCornerradious(sender: self.comingSoonAction)
        }
    }
    //MARK: - Class Methods
    func setContentOffset(position: CGFloat){
        DispatchQueue.main.async {
            UIView.animate(withDuration: 1, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                self.scrollViewOutlet.contentOffset.x = position
            }, completion: nil)
        }
    }
    func getURL() {
        showSpinner()
        let parameters = ["sortBy": "1"]
        NetworkAdapter.methodPOST(.nowShowing(model: parameters), successCallback: {(response) in
            do {
                let getJSON: Any = try JSONSerialization.jsonObject(with: response.data, options: [])
                print(getJSON)
                let resultArray = getJSON as! [String : Any]
                let data1 = resultArray["Results"] as! [[String : Any]]
                                print(data1)
                //                let data = ("\(data1)")
                //                print(data)
                //let str = data["id"] as? [String: Any]
                if !data1.isEmpty{
                    if !self.nowShowName.isEmpty{self.nowShowName.removeAll()}
                    self.nowShowName.append(contentsOf: data1)
                    DispatchQueue.main.async {
                     //   self.videosTableView.reloadData()
                        self.videosTableView.setContentOffset(.zero, animated: true)
                         self.videosTableView.reloadData(effect: .roll)
                    }
                    self.getupComingMovies()
                }
            }catch{
                print("getJSON")
                self.videosTableView.EmptyMessage(message: "No movies Found")
            }
        }, error: {(Error) in
            print("error", Error.localizedDescription)
        }, failure: {(Error) in
            print("error", Error.localizedDescription)
        })
    }
    
    func getLikeStatus(getCount: Int) -> Int{
        var setCount = 0
        if  getCount == 0{
            setCount = 1
        }
        return setCount
    }
    
    //MARK: - Like Action
    @objc func addLikeAction(sender: DOFavoriteButton){
                if !TAMUser.isUserLoggedIn{
                    // show alert
                    showLoginAlert(tabBarController: tabBarController!, toIndex: 1, fromIndex: (tabBarController?.selectedIndex)!)
                                return
                }
            else if !TAMUser.isUserLoggedIn == true{
                    var getSubObj = nowShowName[sender.tag]
        print( getSubObj)
        if let getLikeCount = getSubObj["movie_liked_by_user"] as? [String: Any], let currCount = getLikeCount["liked"] as? Int,  let movie_id = getSubObj["id"] as? Int{
            let parameters = ["movie_id": movie_id, "user_id":   TAMUser.currentUser?.id ?? Int(), "likeCount": getLikeStatus(getCount: currCount)] as [String : Any]
            print(parameters)
            NetworkAdapter.methodPOST(.addLike(model: parameters), successCallback: {(response) in
                do {
                    let getJSON: Any = try JSONSerialization.jsonObject(with: response.data, options: [])
                    let resultArray = getJSON as! [String : Any]
                    print(resultArray)
                    if resultArray["Status"] as? Bool == false{
                        let data1 = resultArray["Results"] as! [Any]
                        SHOWAlert.setinfo(title: "Results Empty..!", subTitle: (data1[0] as? String)!)
                        return
                    }
                    if let data1 = resultArray["Results"] as? [String: Any], !data1.isEmpty, let getCount = data1["likeCount"] as? Int{
                        var getCurr = getLikeCount
                        getCurr["liked"] = getCount
                        getSubObj["movie_liked_by_user"] = getCurr
                        self.nowShowName[sender.tag] = getSubObj
                        if getCount == 0{
                            // unselect
                            sender.isSelected = false
                            // sender.deselect()
                        }else{
                            // select
                            sender.isSelected = true
                            //   sender.select()
                        }
                    }
                }catch{
                    print("applyJSON")
                }
            }, error: {(Error) in
                print("error", Error.localizedDescription)
            }, failure: {(Error) in
                print("error", Error.localizedDescription)
            })
                    }}
    }
    //MARK: - upcoming Methods
    func getupComingMovies(){
        SwiftSpinner.show(duration: 3.0, title: "Loading...")
        let paramss = ["user_id": TAMUser.currentUser?.id] 
        NetworkAdapter.methodPOST(.comingsoon(model: paramss), successCallback: {(response) in
            do {
                let getJSON: Any = try JSONSerialization.jsonObject(with: response.data, options: [])
                print(getJSON)
                let resultArr = getJSON as! [String: Any]
                print(resultArr)
                
                if resultArr["Status"] as? String == "Failed"{
                    let datum = resultArr["Results"] as! [Any]
                    SHOWAlert.setinfo(title: "Results Empty..!", subTitle: (datum[0] as? String)!)
                    return
                }
                let data2 = resultArr["Results"] as! [[String: Any]]
                print(data2)
                if !data2.isEmpty{
                    if !self.upComeName.isEmpty{ self.upComeName.removeAll()}
                    self.upComeName.append(contentsOf: data2)
                    DispatchQueue.main.async {
                       // self.upcomingTable.reloadData()
                         self.upcomingTable.reloadData(effect: .LeftAndRight)
                    }
                }
            }catch{
                print("getJSON")
                self.upcomingTable.EmptyMessage(message: "No movies Found")
            }
        }, error: { (error) in
            // show error from server
            print("error", error.localizedDescription)
        }, failure: { (error) in
            // show Moya error
            print("error", error.localizedDescription)
        })
    }
    
    //MARK: - Action Methods
    @IBAction func nowShowingBtn(_ sender: UIButton) {
        updateCornerradious(sender: sender)
        getURL()
        self.scrollViewOutlet.setContentOffset(CGPoint.zero, animated: true)
    }
    @IBAction func comingSoonBtn(_ sender: UIButton) {
        updateCornerradious(sender: sender)
        getupComingMovies()
        print(self.scrollViewOutlet.frame.origin.y)
        self.scrollViewOutlet.setContentOffset(CGPoint(x: self.getDeviceWidth(), y: 0), animated: true)
    }
    @IBAction func filterAction(_ sender: UIButton) {
        let newVC: FilterViewController = UIStoryboard(storyboard: .main).instantiateViewController()
        newVC.delegate = self
        self.appUtilityPush(newVC)
    }
    
    @IBAction func exclusiveAction(_ sender: UIButton) {
        updateCornerradious(sender: sender)
    }
    //MARK: - handleFilteraction
    func passFilter(currntObj: [String : Any], reset: Bool) {
        //  filterContainerView.isHidden = true
        if !currntObj.isEmpty{
            // call filterapi
            print(currntObj)
            updateFilterSelection(currntObj: currntObj)
        }else if !reset{
            getURL()
        }
    }
    @objc func updateFilterSelection(currntObj: [String : Any]){
        showSpinner()
        NetworkAdapter.methodPOST(.nowShowing(model: currntObj), successCallback: {(response) in
            do {
                let getJSON: Any = try JSONSerialization.jsonObject(with: response.data, options: [])
                print(getJSON)
                let resultArray = getJSON as! [String : Any]
                if resultArray["Status"] as? String == "Failed"{
                    let data1 = resultArray["Results"] as! [Any]
                    SHOWAlert.setinfo(title: "Results Empty..!", subTitle: (data1[0] as? String)!)
                    return
                }
                let data1 = resultArray["Results"] as! [[String : Any]]
                //  print(resultArray)
                if !data1.isEmpty{
                    if !self.nowShowName.isEmpty{self.nowShowName.removeAll()}
                    self.nowShowName.append(contentsOf: data1)  //
                    // self.nowShowName = data1
                    DispatchQueue.main.async()  {
                      //  self.videosTableView.reloadData()
                          self.videosTableView.reloadData(effect: .roll)
                    }
                }
            }catch{
                print("applyJSON")
            }
        }, error: {(Error) in
            print("error", Error.localizedDescription)
        }, failure: {(Error) in
            print("error", Error.localizedDescription)
        })
        
    }
    
    //MARK: - TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count:Int?
        if tableView == self.videosTableView {
            count = nowShowName.count
        }
        if tableView == self.upcomingTable {
            count = upComeName.count
            // return 2
            // count =  upcomingImags.count
        }
        return count!
        //  return nowShowName.count || upcomingImags.count // tableView == self.videosTableView  ? nowShowImage.count : movieImg.count
    }
    
    
    
//    @available(iOS 11.0, *)
//    func tableView(_ tableView: UITableView, shouldSpringLoadRowAt indexPath: IndexPath, with context: UISpringLoadedInteractionContext) -> Bool{
//        return true
//    }
    var scrollOrientation: UIImageOrientation?
    var lastPos = CGPoint.zero
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollOrientation = scrollView.contentOffset.y > lastPos.y ? .down : .up
        lastPos = scrollView.contentOffset
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if tableView.isDragging {
            let myView: UIView = cell.contentView
            let layer: CALayer = myView.layer
            var rotationAndPerspectiveTransform: CATransform3D = CATransform3DIdentity
            rotationAndPerspectiveTransform.m34 = 1.0 / -1000
             let offsetPositioning = CGPoint(x: 0, y: cell.contentView.frame.size.height * 4)
            if scrollOrientation == .down {
                rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, .pi * 0.5, 1.0, 0.0, 0.0)
                 rotationAndPerspectiveTransform = CATransform3DTranslate(rotationAndPerspectiveTransform, offsetPositioning.x, offsetPositioning.y, 50.0)
            } else {
                rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, -.pi * 0.5, 1.0, 0.0, 0.0)
                rotationAndPerspectiveTransform = CATransform3DTranslate(rotationAndPerspectiveTransform, offsetPositioning.x, offsetPositioning.y, -50.0)
            }
            myView.layer.opacity = 0.8
            layer.transform = rotationAndPerspectiveTransform
            UIView.animate(withDuration: 0.65, delay: 0, usingSpringWithDamping: 0.85, initialSpringVelocity: 0.8, options: [], animations: {
                myView.layer.transform = CATransform3DIdentity
                myView.layer.opacity = 1
            }) { finished in
            }
        }
         /*
        let cellContentView: UIView = cell.contentView
        let rotationAngleDegrees: CGFloat = -30
        let rotationAngleRadians: CGFloat = rotationAngleDegrees * (.pi / 180)
        let offsetPositioning = CGPoint(x: 0, y: cell.contentView.frame.size.height * 4)
        var transform: CATransform3D = CATransform3DIdentity
        transform = CATransform3DRotate(transform, rotationAngleRadians, -50.0, 0.0, 1.0)
        transform = CATransform3DTranslate(transform, offsetPositioning.x, offsetPositioning.y, -50.0)
        cellContentView.layer.transform = transform
        cellContentView.layer.opacity = 0.8
        
        UIView.animate(withDuration: 0.65, delay: 0, usingSpringWithDamping: 0.85, initialSpringVelocity: 0.8, options: [], animations: {
            cellContentView.layer.transform = CATransform3DIdentity
            cellContentView.layer.opacity = 1
        }) { finished in
        } */
        
        /* 2 animation
         cell.alpha = 0
         let transform  = CATransform3DTranslate(CATransform3DIdentity, -250, 20, 0)
         cell.layer.transform = transform
         UIView.animate(withDuration: 1.0){
         cell.alpha = 1
         cell.layer.transform = CATransform3DIdentity
         } */
        
        //1. Setup the CATransform3D structure
        /*  side scroll
         var rotation: CATransform3D
         rotation = CATransform3DMakeRotation((90.0 * .pi) / 180, 0.0, 0.7, 0.4)
         rotation.m34 = 1.0 / -600
         //2. Define the initial state (Before the animation)
         cell.layer.shadowColor = UIColor.black.cgColor
         cell.layer.shadowOffset = CGSize(width: 10, height: 10)
         cell.alpha = 0
         cell.layer.transform = rotation
         cell.layer.anchorPoint = CGPoint(x: 0, y: 0.5)
         //3. Define the final state (After the animation) and commit the animation
         UIView.beginAnimations("rotation", context: nil)
         UIView.setAnimationDuration(0.8)
         cell.layer.transform = CATransform3DIdentity
         cell.alpha = 1
         cell.layer.shadowOffset = CGSize(width: 0, height: 0)
         UIView.commitAnimations() */
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: HomeTableViewCell!
        var cell1: UpcomingTableViewCell!
        if tableView == videosTableView {
            cell = tableView.dequeueReusableCell(withIdentifier: "MoviesCell", for: indexPath) as! HomeTableViewCell
            if !nowShowName.isEmpty{
                let getSubObj = nowShowName[indexPath.row]
                self.loadImageFromCell(imgStr: String(format:"%@%@",TAMBaseURL.baseURL, (getSubObj["movie_backgroundimg"] as? String)!), imgViewNam: cell.moviesImages)
                cell.movieNameLabel.text =  getSubObj["name"] as? String ?? ""
                cell.languageLabel.text = getSubObj["movie_language"] as? String ?? ""
                cell.viewsLabel.text = String(format:"%d Views", (getSubObj["movie_votes"] as? Int ?? 0))
                cell.votesPercentage.text = String(format:"%@ %@",(getSubObj["movie_average"]  as? String)!, "%")
                cell.votesOutlet.tag = indexPath.row
                cell.votesOutlet.addTarget(self, action: #selector(addLikeAction), for: .touchUpInside)
                if let getCertificate = getSubObj["movie_certificate"] as? [String: Any] {
                    cell.censorCertifyLabel.text = getCertificate["movie_certificate"] as? String
                }
            }
            cell.censorCertifyLabel.layer.cornerRadius = cell.censorCertifyLabel.frame.width / 2
            cell.censorCertifyLabel.layer.borderWidth = 1
            cell.censorCertifyLabel.layer.borderColor = UIColor.white.cgColor     // (red: 57 / 255, green: 57 / 255, blue: 57 / 255, alpha: 1).cgColor
        }
        else if tableView == upcomingTable {
            cell1 = upcomingTable.dequeueReusableCell(withIdentifier: "UpcomingCell", for: indexPath) as! UpcomingTableViewCell
            cell1.notifyOutlet.layer.cornerRadius = 15
            cell1.notifyOutlet.layer.borderWidth = 1
            cell1.notifyOutlet.layer.borderColor = UIColor.white.cgColor
            if !self.upComeName.isEmpty{
                let getUpcoming = self.upComeName[indexPath.row]
                cell1.upComingLanguage.text = getUpcoming["movie_language"] as? String ?? ""
                cell1.upComingMovieName.text = getUpcoming["movie_original_title"] as? String ?? ""
                self.loadImageFromCell(imgStr: String(format: "%@%@", TAMBaseURL.baseURL, (getUpcoming["movie_backgroundimg"] as? String)!), imgViewNam: cell1.upcomingImagesView)
            }
            return cell1
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if tableView == videosTableView{
            let selectedCinemas = self.nowShowName[indexPath.row]
            openDetailVC(selectedCinemas: selectedCinemas)
        }
        if tableView == upcomingTable{
            let selectedCinemas = self.upComeName[indexPath.row]
            openDetailVC(selectedCinemas: selectedCinemas)
        }
    }
    
    //MARK: - get Image from Previous Data
    func openDetailVC(selectedCinemas: [String: Any]){
        if !selectedCinemas.isEmpty{
            let detailVC: MovieDetailViewController = UIStoryboard(storyboard: .main).instantiateViewController()
            detailVC.getPreviousData = selectedCinemas
            self.appUtilityPush(detailVC)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let baseTabBarVC = segue.destination as? FilterViewController {
            baseTabBarVC.delegate = self
            
        }
 }
 }
