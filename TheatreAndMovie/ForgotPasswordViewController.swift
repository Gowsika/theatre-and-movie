//
//  ForgotPasswordViewController.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 07/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
@IBOutlet weak var emailAddressText: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
       self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        emailAddressText.layer.borderWidth = 1
        emailAddressText.layer.cornerRadius = 16
        emailAddressText.layer.masksToBounds = true
        let padding1 = UIImageView(frame: CGRect(x: 10, y: 0, width: 25, height: 19))
        padding1.image = UIImage(named: "mail_icon-2")
        padding1.contentMode = .scaleAspectFit
        let test1 = UIView(frame: CGRect(x: 10, y: 0, width: 25, height: 19))
        test1.addSubview(padding1)
        emailAddressText.leftView?.frame = padding1.frame
        emailAddressText.leftView = test1
        emailAddressText.leftViewMode = .always
        let navigationBar = navigationController?.navigationBar
        navigationBar?.barTintColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
        navigationBar?.isTranslucent = false
        navigationBar?.tintColor = UIColor.red
        navigationItem.title = "Forgot Username/Password?"
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
   override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
   }
    @IBAction func submitAction(_ sender: UIButton) {
    }
    

}
