//
//  CellReviewTableViewCell.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 27/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class CellReviewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var daysAgoLabel: UILabel!
    @IBOutlet weak var reviewShownLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        self.reviewShownLabel.sizeToFit()
        self.reviewShownLabel.layoutIfNeeded()
        }
override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
 
    }
    
    

}
