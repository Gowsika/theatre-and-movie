//
//  MainViewController.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 25/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import GoogleSignIn
class MainViewController: UIViewController,GIDSignInDelegate,GIDSignInUIDelegate {
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.textAlignment = .center
        numberTextField.textAlignment = .center
        passwordTextField.textAlignment = .center
        emailTextField.textAlignment = .center
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        nameTextField.layer.borderWidth = 1
        passwordTextField.layer.borderWidth = 1
        numberTextField.layer.borderWidth = 1
        emailTextField.layer.borderWidth = 1
        nameTextField.layer.cornerRadius = 16
        nameTextField.layer.masksToBounds = true
        passwordTextField.layer.cornerRadius = 16
        passwordTextField.layer.masksToBounds = true
        emailTextField.layer.cornerRadius = 16
        emailTextField.layer.masksToBounds = true
        numberTextField.layer.cornerRadius = 16
        numberTextField.layer.masksToBounds = true
  let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tap)
        let padding = UIImageView(frame: CGRect(x: 10, y: 0, width: 25, height: 19))
        padding.image = UIImage(named: "user_icon-2")
        padding.contentMode = .scaleAspectFit
        let test = UIView(frame: CGRect(x: 10, y: 0, width: 25, height: 19))
        test.addSubview(padding)
        nameTextField.leftView?.frame = padding.frame
        nameTextField.leftView = test
        nameTextField.leftViewMode = .always
        let padding1 = UIImageView(frame: CGRect(x: 10, y: 0, width: 25, height: 19))
        padding1.image = UIImage(named: "mail_icon-2")
        padding1.contentMode = .scaleAspectFit
        let test1 = UIView(frame: CGRect(x: 10, y: 0, width: 25, height: 19))
        test1.addSubview(padding1)
        emailTextField.leftView?.frame = padding1.frame
        emailTextField.leftView = test1
        emailTextField.leftViewMode = .always


        let padded = UIImageView(frame: CGRect(x: 10, y: 0, width: 25, height: 19))
        padded.image = UIImage(named: "mobile_icon-2")
        padded.contentMode = .scaleAspectFit
        let tested = UIView(frame: CGRect(x: 10, y: 0, width: 25, height: 19))
        tested.addSubview(padded)
        numberTextField.leftView?.frame = padded.frame
        numberTextField.leftView = tested
        numberTextField.leftViewMode = .always
        getPadding()
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    @objc func hideKeyboard(){
                view.endEditing(true)
            }
    
    func getPadding(){
                let enView = UIImageView(frame: CGRect(x: 10, y: 0, width: 25, height: 19))
                enView.image = UIImage(named: "pass_icon-2")
                enView.contentMode = .scaleAspectFit
                let testView = UIView(frame: CGRect(x: 10, y: 0, width: 25, height: 19))
                testView.addSubview(enView)
                passwordTextField.leftView?.frame = enView.frame
                passwordTextField.leftView = testView
                passwordTextField.leftViewMode = .always
            }
    @IBAction func signUpAction(_ sender: UIButton) {
        let nav = storyboard?.instantiateViewController(withIdentifier: "MovieDetailViewController") as! MovieDetailViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    @IBAction func faceBookLogin(_ sender: UIButton) {
        FacebookSDK.FaceBook()
    }
    
    @IBAction func googleLogin(_ sender: UIButton) {
        GoogleSDK.GooglePlus()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        let login = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
               self.navigationController?.pushViewController(login, animated: true)
    }
    
    @IBAction func skipAction(_ sender: UIButton) {
        let skip = storyboard?.instantiateViewController(withIdentifier: "MainTabBarViewController") as! MainTabBarViewController
        self.navigationController?.pushViewController(skip, animated: true)
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil{
            print(error ?? "google error")
            return
        }
    }
}
