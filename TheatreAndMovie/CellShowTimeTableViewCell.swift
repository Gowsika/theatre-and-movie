//
//  CellShowTimeTableViewCell.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 29/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class CellShowTimeTableViewCell: UITableViewCell {

    @IBOutlet var showTimes: [UIButton]!
    @IBOutlet weak var morBtnOutlet: UIButton!
    @IBOutlet weak var theatreImage: UIImageView!
    @IBOutlet weak var nightBtnOutlet: UIButton!
    @IBOutlet weak var eveBtnOutlet: UIButton!
    @IBOutlet weak var matActOutlet: UIButton!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var theatreNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        updateCornerRadius()
        theatreImage.layer.cornerRadius = 10
        theatreImage.layer.masksToBounds = true
      }
    func updateCornerRadius(){
        for sender in showTimes{
            sender.layer.cornerRadius = 10
            sender.layer.borderColor = UIColor.lightGray.cgColor
            sender.layer.masksToBounds = true
            sender.layer.borderWidth = (1.0 /  UIScreen.main.scale)
            sender.isHidden = true
        }
             }
     override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func updateShowTime(getTimeData: [[String: Any]]){
        if getTimeData.isEmpty{
           return
        }
        for (index,element) in getTimeData.enumerated(){
            showTimes[index].isHidden = false
            showTimes[index].setTitle(element["time"] as? String, for: .normal)
            if  element["type"] as? String == "Male"{
                showTimes[index].backgroundColor = UIColor.gray
                 }
            else  if  element["type"]  as? String == "Family"{
                showTimes[index].backgroundColor = UIColor(red: 217 / 255, green: 86 / 255, blue: 72 / 255, alpha: 1)
            }
           
        }
    }
   }
