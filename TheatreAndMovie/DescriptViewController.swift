//
//  DescriptViewController.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 27/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class CategoryRow : UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    var getSubCategory = [[String: Any]]()
    var getCrewURL: String = ""
     let collectionCellIdentifier = "castCell"
    override func awakeFromNib() {
        self.collectionView.dataSource = self
    }
         func loadCollectionView(subCategory:[[String: Any]]) {
         self.getSubCategory = subCategory
         self.collectionView.reloadData()
    }
}
class CastCell : UICollectionViewCell {
    @IBOutlet weak var imageCast: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
         imageCast.layer.cornerRadius = 8.0
    }
    
}

extension CategoryRow : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return getSubCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionCellIdentifier, for: indexPath) as! CastCell
         let getSubcast = getSubCategory[indexPath.row]
        var getTitle = ""
        var ImageTitle = ""
        if let getTit = getSubcast["crew_fname"] as? String {
            getTitle = getTit
           // man copy1
        ImageTitle = String(format:"%@%@%@",TAMBaseURL.baseURL,getCrewURL,(getSubcast["crew_image"] as? String)!)
        }
        else if let getTitl = getSubcast["name"] as? String {
            getTitle = "\(getTitl) \r \(getSubcast["role"] as? String ?? "")"
            ImageTitle = ""
        }
        //  cell.loadImageFromCast(imgStr: ImageTitle, imgViewNam:  cell.imageCast)
        cell.loadImageFromCrew(imgStr: ImageTitle, imgViewName: cell.imageCast)
         cell.lblTitle.text = getTitle
        return cell
    }
    
}
/*
extension CategoryRow : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsPerRow:CGFloat = 4
        let hardCodedPadding:CGFloat = 5
        let itemWidth = (collectionView.bounds.width / itemsPerRow) - hardCodedPadding
        let itemHeight = collectionView.bounds.height - (2 * hardCodedPadding)
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
}
*/

extension CategoryRow : UICollectionViewDelegate { }


class DescriptViewController: UIViewController  {
    
    var getDescription = [[String: Any]]()
    var getMovieId: String!
    let tableCellIdentifier = "TableCell"
    
    @IBOutlet weak var descriptiveTable: UITableView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var moreLessOutlet: UIButton!
    @IBOutlet weak var decripeTextView: UITextView!
    var getCastURL: String = ""
    //MARK: - Load Methods
    override func viewDidLoad() {
        descriptiveTable.removeEmptyCell()
        super.viewDidLoad()
        
        if let getID = getMovieId{
           //  descriptiveTable.rowHeight = UITableViewAutomaticDimension
//descriptiveTable.estimatedRowHeight = 107.0
            getMovieDescript(getMovieId: getID)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Get Description
    func getMovieDescript(getMovieId: String){
        let parrams = ["movie_id": getMovieId]
        NetworkAdapter.methodPOST(.getDescript(model: parrams as Any as! [String : Any]), successCallback: {(response) in
            do {
                let getJSON: Any = try JSONSerialization.jsonObject(with: response.data, options: [])
                let resultArr = getJSON as! [String: Any]
             //    print(resultArr)
                self.getDescription.removeAll()
                if let data = resultArr["Results"] as? [String: Any], !data.isEmpty{
                    self.decripeTextView.text = data["movie_description"] as? String ?? ""
                    self.getCastURL = data["castBaseUrl"] as? String ?? ""
                    for (key, value) in data{
                        if value is [[String: Any]]  && key != "category"{
                         var temp = [String:[[String: Any]]]()
                            temp[key] =  value as? [[String : Any]]
                              print(temp)
                            self.getDescription.append(temp)
                            print(self.getDescription)
                        }
                }
                    DispatchQueue.main.async {
                        self.descriptiveTable.reloadData()
                        //self.descriptiveTable.contentOffset = .zero
                    }
                }
            }catch{
                print("getJSON")
            }
        }, error: {(Error) in
            print("error", Error.localizedDescription)
        }, failure: {(Error) in
            print("error", Error.localizedDescription)
        })
    }
    }

extension DescriptViewController{
    
    //MARK: - moreLessAction
    @IBAction func moreLessAction(_ sender: UIButton) {
        var title = "Less"
        var height: CGFloat = 70.0
        if sender.tag == 0{
            height = self.getRowHeightFromText(strText: self.decripeTextView.text)
            sender.tag = 1
        }else {
            title  = "More"
            sender.tag = 0
        }
       UIView.animate(withDuration: 1.0, animations: {
            self.heightConstraint.constant = height
            self.moreLessOutlet.setTitle(title, for: .normal)
            self.view.layoutIfNeeded()
            })
    }
        func getRowHeightFromText(strText : String!) -> CGFloat{
        let textView : UITextView! = UITextView(frame: CGRect(x: self.decripeTextView.frame.origin.x, y: 0, width: self.decripeTextView.frame.size.width, height: 0))
        textView.text = strText
       // textView.font = UIFont(name: "Fira Sans", size: 16.0)
        textView.sizeToFit()
        var textFrame : CGRect! = CGRect()
        textFrame = textView.frame
        var size : CGSize! = CGSize()
        size = textFrame.size
        size.height = 30 + textFrame.size.height
        return size.height
    }
}

extension DescriptViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 55.0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return  getDescription[section].keys.first?.capitalizingFirstLetter()
    }
    func  tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 107.0
     }
    func numberOfSections(in tableView: UITableView) -> Int {
        return getDescription.count
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.contentView.backgroundColor = .white
            headerView.textLabel?.textColor = .black
        }
        
    }
}

extension DescriptViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tableCellIdentifier , for: indexPath) as! CategoryRow
          let key = getDescription[indexPath.section].keys.first
        if let getSubCate = getDescription[indexPath.section][key!] as? [[String: Any]]{
            cell.getCrewURL = getCastURL
           // cell.frame = tableView.bounds
          //   cell.layoutIfNeeded()
            cell.loadCollectionView(subCategory: getSubCate)
          //  cell.collectionView.constant = cell.collectionView.contentSize.height
        }
        return cell
    }
    
 }




