//
//  CellTheatreDetailsTableViewCell.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 02/07/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class CellTheatreDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var eveningShowLabel: UIButton!
    @IBOutlet weak var morningLabel: UIButton!
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var moviesName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        eveningShowLabel.layer.cornerRadius = 10
        eveningShowLabel.layer.masksToBounds = true
        eveningShowLabel.layer.borderWidth = 0.7
        eveningShowLabel.layer.borderColor = UIColor.lightGray.cgColor
        
        morningLabel.layer.cornerRadius = 10
        morningLabel.layer.masksToBounds = true
        morningLabel.layer.borderWidth = 0.7
        morningLabel.layer.borderColor = UIColor.lightGray.cgColor
       }
override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
