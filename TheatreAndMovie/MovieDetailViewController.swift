//
//  MovieDetailViewController.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 26/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import MediaPlayer
import AVKit
import MediaToolbox
import youtube_ios_player_helper

class MovieDetailViewController: UIViewController,YTPlayerViewDelegate {
 let color = UIColor(red: 134/255, green: 135/255, blue: 136/255, alpha: 1)
    var getPreviousData = [String: Any]()
    var getPreviousMovieName = [String: Any]()
    var getPreMovieDetails = [[String: Any]]()
     let colors = Colors()
    @IBOutlet weak var showTimesContainView: UIView!
    @IBOutlet weak var descripeContainView: UIView!
    @IBOutlet weak var scrollCategories: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var viewsLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var movieNameLabel: UILabel!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var censorCertifyLabel: UILabel!
    @IBOutlet weak var reviewsLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var showTimeLabel: UILabel!
    @IBOutlet weak var reviewsOutlet: UIButton!
    @IBOutlet weak var descriptionOutlet: UIButton!
    @IBOutlet weak var showTimesOutlet: UIButton!
    @IBOutlet weak var playImage: UIImageView!
    @IBOutlet weak var playerView: YouTubePlayerView!
     @IBOutlet weak var bgView: UIView!
   @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var likeButton: DOFavoriteButton!
    //MARK: - Load Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setBottomLine()
        /*
         comedyLabel.layer.borderWidth = 0.5
         comedyLabel.layer.cornerRadius = 12
         comedyLabel.layer.masksToBounds = true
         actionLabel.layer.borderWidth = 0.5
         actionLabel.layer.cornerRadius = 12
         actionLabel.layer.masksToBounds = true
         thrillerLabel.layer.borderWidth = 0.5
         thrillerLabel.layer.cornerRadius = 12
         thrillerLabel.layer.masksToBounds = true
         scienceFictionLabel.layer.borderWidth = 0.5
         scienceFictionLabel.layer.cornerRadius = 12
         scienceFictionLabel.layer.masksToBounds = true */
        
        print(getPreviousData)
        if  !getPreviousData.isEmpty{
                         durationLabel.text = self.secondsToHMS(seconds: (getPreviousData["movie_length"] as? Int)! )
            movieNameLabel.text = getPreviousData["name"]  as? String
            languageLabel.text = getPreviousData["movie_language"] as? String
            viewsLabel.text = String(format:"%d Views",(getPreviousData["movie_votes"]  as? Int)!)
            percentageLabel.text = String(format:"%@ %@",(getPreviousData["movie_average"]  as? String)!, "%")
          //  likeButton.addTarget(self, action: #selector(getPreviousData[""]), for: .touchUpInside)
            self.loadImageFromCell(imgStr: String(format:"%@%@",TAMBaseURL.baseURL, (getPreviousData["movie_backgroundimg"] as? String)!), imgViewNam:  self.playImage)
            getGrandient()
           if let getTrailer = getPreviousData["movie_trailer"] as? String, !getTrailer.isEmpty , let getTrailerName = getTrailer.components(separatedBy: ",").first, !getTrailerName.isEmpty {
               // self.playerView.delegate = self
                DispatchQueue.main.async() {
                    // your UI update code
                  // self.playerView.webView?.scalesPageToFit = true
                    if #available(iOS 11.0, *) {
                        self.playerView.webView?.scrollView.contentInsetAdjustmentBehavior = .never
                    } else {
                        // Fallback on earlier versions
                    }
                    self.playerView.playerVars = [
                        "modestbranding" : "1",
                        "controls" : "2",
                        "playsinline" : "1",
                        "autohide" : "1",
                        "showinfo" : "0",
                        "autoplay" : "1",
                        "fs" : "1",
                        "rel" : "0",
                        "loop" : "0",
                        "enablejsapi" : "1",
                        "iv_load_policy": "3"
                    ] as YouTubePlayerView.YouTubePlayerParameters
                    self.playerView.loadVideoID(getTrailerName)
                    
                   // self.playerView.frame = self.playImage.frame
                   // self.playerView.bringSubview(toFront: self.playImage)
                }
             }
            generatecategory(category: getPreviousData["category"]  as? [Any] as! [[String : Any]])
}
        descriptionLabel.isHidden = true
        reviewsLabel.isHidden = true
        containerView.isHidden = true
        descripeContainView.isHidden = true
        showTimesOutlet.setTitleColor(UIColor.red, for: .normal)
        censorCertifyLabel.layer.cornerRadius = censorCertifyLabel.frame.width / 2
        censorCertifyLabel.layer.borderWidth = 0.5
        censorCertifyLabel.layer.borderColor = UIColor.black.cgColor
             }
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
}
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //  NotificationCenter.default.addObserver(self, selector: #selector(deviceOrientationDidChange), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        UIApplication.shared.isStatusBarHidden = true
        showAndHidenavigationBar(status: false)
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        bar.setBackgroundImage(UIImage(), for: .default)
        bar.shadowImage = UIImage()
        bar.backgroundColor = UIColor(red: 0.0, green: 0.3, blue: 0.5, alpha: 0)
    }
override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.isStatusBarHidden = false
        showAndHidenavigationBar(status: true)
        //   NotificationCenter.default.removeObserver(self)
    }
         override var prefersStatusBarHidden: Bool {
        return true
    }
    func setBottomLine(){
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x: 0.0, y: 37, width: bgView.frame.size.width + 50, height: 1.0)
        bottomBorder.backgroundColor = UIColor(white: 0.8, alpha: 1.0).cgColor
        bgView.layer.addSublayer(bottomBorder)
    }
          //MARK: - Class Methods
    func getGrandient(){
        DispatchQueue.main.async {
            let backGroundLayer = self.colors.gl
            backGroundLayer?.frame = self.playImage.bounds
            self.playImage.layer.insertSublayer(backGroundLayer!, at: UInt32(self.playImage.layer.sublayers?.count ?? 0))
        }
    }
func generatecategory(category:[[String: Any]]){
        if !category.isEmpty{
            print(category)
            
            // only for 4 items
            var xAxis: Float = 5
            for i in 0..<category.count {
                // var size = CGSize.zero
                if i < 5     {
                    let getTitle = category[i]["category_name"] as? String
                    let font = UIFont.systemFont(ofSize: 12)
                    let width = (getTitle?.size(withAttributes: [NSAttributedStringKey.font: font]).width)! + 10
                    let label = UILabel(frame: CGRect(x: CGFloat(xAxis), y: 5, width:  width , height: 22))
                    label.font = UIFont(name: "SF Compact Display", size: 12)
                    if i > 2   { //(i != 0 && i % 3 == 0)
                        label.frame.origin.y = label.frame.origin.y + label.frame.size.height + 10
                        if i == 3{
                            xAxis   = 5
                        }
                        label.frame.origin.x = CGFloat(xAxis)
                    }
                    label.text = getTitle
                    label.font = font
                    label.textAlignment = .center
                    // size = label.intrinsicContentSize
                    label.backgroundColor = UIColor(red: 237 / 255, green: 237 / 255, blue: 237 / 255, alpha: 1)
                    label.layer.cornerRadius = 10
                    label.layer.borderColor = UIColor.lightGray.cgColor
                    label.layer.borderWidth = 1.0
                    label.clipsToBounds = true
                    scrollCategories.addSubview(label)
                    // Increase yAxis
                    xAxis = Float(CGFloat(xAxis) + width + 5)
                }
            }
        }
    }
    func getLikeStatus(getCount: Int) -> Int{
        var setCount = 0
        if  getCount == 0{
            setCount = 1
        }
        return setCount
    }
    @objc func addLikeAction(sender: DOFavoriteButton){
        if !TAMUser.isUserLoggedIn{
            // show alert
            SHOWAlert.getSuccessAlert(title: "CinemaTime", subTitle: "Login To Proceed", backgroundColor: .red)
           // showLoginAlert(tabBarController: tabBarController!, toIndex: 1, fromIndex: (tabBarController?.selectedIndex)!)
            return
        }
        else  {
            let getLikeCount = getPreviousData["movie_liked_by_user"] as? [String: Any]
            let currCount = getLikeCount!["liked"] as? Int
            let parameters = ["movie_id": getPreviousData["id"] as? Int, "user_id":   TAMUser.currentUser?.id, "likeCount": getLikeStatus(getCount: currCount!)]
              print(parameters)
            NetworkAdapter.methodPOST(.addLike(model: parameters), successCallback: {(response) in
                    do {
                        let getJSON: Any = try JSONSerialization.jsonObject(with: response.data, options: [])
                        let resultArray = getJSON as! [String : Any]
                        print(resultArray)
                        if resultArray["Status"] as? Bool == false{
                            let data1 = resultArray["Results"] as! [Any]
                            SHOWAlert.setinfo(title: "Results Empty..!", subTitle: (data1[0] as? String)!)
                            return
                        }
                        if let data1 = resultArray["Results"] as? [String: Any], !data1.isEmpty, let getCount = data1["likeCount"] as? Int{
                            print(data1)
//                            var getCurr = getLikeCount
//                            getCurr!["liked"] = getCount
//                            print(getCount)
//                            self.getPreviousData["movie_liked_by_user"] = getCurr
                            
                          //  self.nowShowName[sender.tag] = getSubObj
                            if getCount == 0{
                                // unselect
                                sender.isSelected = false
                                // sender.deselect()
                            }else{
                                // select
                                sender.isSelected = true
                                //   sender.select()
                            }
                        }
                    }catch{
                        print("applyJSON")
                    }
                }, error: {(Error) in
                    print("error", Error.localizedDescription)
                }, failure: {(Error) in
                    print("error", Error.localizedDescription)
                })
            }
    }
//    func generatecategory(category:[[String: Any]]){
//        if !category.isEmpty{
//            print(category)
//
//            // only for 4 items
//            var xAxis: Float = 5
//            for i in 0..<category.count {
//                // var size = CGSize.zero
//                if i < 5 {
//                    let getTitle = category[i]["category_name"] as? String
//                    let font = UIFont.systemFont(ofSize: 12)
//                    let width = (getTitle?.size(withAttributes: [NSAttributedStringKey.font: font]).width)! + 10
//                    let label = UILabel(frame: CGRect(x: CGFloat(xAxis), y: 5, width:  width , height: 22))
//                    print(i)
//                    if   (i > 2) {
//                        label.frame.origin.y = label.frame.origin.y + label.frame.size.height + 10
//                        xAxis   = 5
//                        label.frame.origin.x = CGFloat(xAxis)
//                    }
//                    label.text = getTitle
//                    label.font = font
//                    label.textAlignment = .center
//                    // size = label.intrinsicContentSize
//                    label.backgroundColor = .lightGray
//                    label.layer.cornerRadius = 10
//                    label.layer.borderColor = UIColor.gray.cgColor
//                    label.layer.borderWidth = 1.0
//                    label.clipsToBounds = true
//                    scrollCategories.addSubview(label)
//                    // Increase yAxis
//                    xAxis = Float(CGFloat(xAxis) + width + 5  )
//                }
//
//            }
//            // Do Not forget to set Contentsize of your ScrollView.
//            //   scrollCategories.contentSize = CGSize(width: scrollCategories.frameLayoutGuide, height: CGFloat(yAxis + 20))
//
//        }
//    }
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        self.playImage.isHidden = true
      //  self.playImage.isHidden = true
       // lastReportedYTState = playerView.playerState()
        
    }

    /*
     kYTPlayerStateUnstarted,
     kYTPlayerStateEnded,
     kYTPlayerStatePlaying,
     kYTPlayerStatePaused,
     kYTPlayerStateBuffering,
     kYTPlayerStateQueued,
     kYTPlayerStateUnknown
     */
    //MARK: - Action  Methods
    @IBAction func showTimeAction(_ sender: UIButton) {
        showTimeLabel.isHidden = false
        descriptionLabel.isHidden = true
        reviewsLabel.isHidden = true
        showTimesOutlet.setTitleColor(UIColor.red, for: .normal)
        descriptionOutlet.setTitleColor(color, for: .normal)
        reviewsOutlet.setTitleColor(color, for: .normal)
        containerView.isHidden = true
        descripeContainView.isHidden = true
        showTimesContainView.isHidden = false
    }
    @IBAction func descriptionAction(_ sender: UIButton) {
        showTimeLabel.isHidden = true
        descriptionLabel.isHidden = false
        reviewsLabel.isHidden = true
        descriptionOutlet.setTitleColor(UIColor.red, for: .normal)
        showTimesOutlet.setTitleColor(color, for: .normal)
        reviewsOutlet.setTitleColor(color, for: .normal)
        containerView.isHidden = true
        descripeContainView.isHidden = false
        showTimesContainView.isHidden = true
    }
    @IBAction func reviewsAction(_ sender: UIButton) {
        showTimeLabel.isHidden = true
        descriptionLabel.isHidden = true
        reviewsLabel.isHidden = false
        reviewsOutlet.setTitleColor(UIColor.red, for: .normal)
        showTimesOutlet.setTitleColor(color, for: .normal)
        descriptionOutlet.setTitleColor(color, for: .normal)
        containerView.isHidden = false
        descripeContainView.isHidden = true
        showTimesContainView.isHidden = true
       }
    
    @IBAction func buttonTapped(_ sender: DOFavoriteButton) {
        if sender.isSelected{
            if TAMUser.currentUser?.id == nil{
                likeButton.addTarget(self, action: #selector(addLikeAction), for: .touchUpInside)
                sender.deselect()
              }
                 }else{
             sender.select()
         }
    }
    
//   @IBAction func playAction(_ sender: UIButton) {
//      //  print(playerView.playerState().hashValue)
//        if playerView.playerState().hashValue == 2{
//            playerView.pauseVideo()
//        }else if playerView.playerState().hashValue != 2 {
//             playerView.playVideo()
//        }
//       if sender.currentTitle == "Play"{
//            sender.titleLabel?.text = "Pause"
//        }else{
//           sender.titleLabel?.text = "Play"
//        }
//
//}
override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   if let controller = segue.destination as? ShowTimesViewController{
        if let getID = getPreviousData["id"] as? NSNumber {
            print(getPreviousData)
            controller.getMovieId = String(format:"%@", getID)
            controller.getprevMovieDetails = getPreviousData
                 }
    } else if let controller = segue.destination as? DescriptViewController {
        if let getID = getPreviousData["id"] as? NSNumber {
            controller.getMovieId = String(format:"%@", getID)
        }
   }
        else if let controller = segue.destination as? ReviewViewController{
            if let getID = getPreviousData["id"] as? NSNumber{
                print(getPreviousData)
                controller.getMovieId = String(format: "%@", getID)
            }
    }
//        if let baseTabBarVC = segue.destination as? ReviewViewController{
//
//        print(getPreviousData)
//        if let getID = getPreviousData["id"] as? NSNumber {
//
//        baseTabBarVC.getMovieId = String(format:"%@", getID)
//     }
//              else if let getID = getPreviousData["id"] as? String {
//                baseTabBarVC.getMovieId = getID
//                
//                }
//           }
//
//    }
   
    }
    
    
}
