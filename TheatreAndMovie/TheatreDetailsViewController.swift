//
//  TheatreDetailsViewController.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 26/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import CalendarKit
import DateToolsSwift
class TheatreDetailsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource ,DayViewDelegate   {
    var getTheatreData = [[String: Any]]()
    var getprevTheatreDetails: [String: Any]!
   
  
    @IBOutlet weak var movieTable: UITableView!
    @IBOutlet weak var calendarView: DayView!
    var navTitle: String!
    var setCurrentdate : String = ""
    //MARK: - Load Methods
    override func viewDidLoad() {
        movieTable.removeEmptyCell()
        super.viewDidLoad()
        navigationItem.title = navTitle
        navigationController?.navigationBar.tintColor = UIColor.white
        calendarView.delegate = self
        calendarView.backgroundColor = .white
        print(getprevTheatreDetails)
        callTheatre(getDate: Date())
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
     func callTheatre(getDate: Date){
        setCurrentdate = getCurrentDate(getDate: getDate)
        getTheatreDetails()
    }
    func getTherateParams() -> [String: Any]{
        if let gettemp = getprevTheatreDetails, !gettemp.isEmpty{
            print(["theatre_id" : gettemp["id"]  as? Int ?? 1, "date": setCurrentdate, "audienceType": 0])
            return ["theatre_id" : gettemp["id"]  as? Int ?? 1, "date": setCurrentdate, "audienceType": 0]
        }else{
            return [:]
        }
    }
        //MARK: Class Methods
    func getTheatreDetails(){
        showSpinner()
        print(getTherateParams())
    NetworkAdapter.methodPOST(.getTheatredetail(model: getTherateParams()), successCallback: {(response) in
            do {
                let getJSON: Any = try JSONSerialization.jsonObject(with: response.data, options: [])
                let resultArray = getJSON as! [String : Any]
                print(resultArray)
                if  !self.getTheatreData.isEmpty{self.getTheatreData.removeAll()}
                if let data = resultArray["Results"] as? [[String : Any]], !data.isEmpty{
                    self.getTheatreData.append(contentsOf: data)
                }
                DispatchQueue.main.async {
                    self.movieTable.reloadData()
                }
            }catch{
                print("getJSON")
                 self.movieTable.EmptyMessage(message: "No Movies Found")
            }
        }, error: {(Error) in
            print("error", Error.localizedDescription)
        }, failure: {(Error) in
            print("error", Error.localizedDescription)
        })
    }
    
    @IBAction func filterAction(_ sender: UIBarButtonItem) {
        
    }
    
    //MARK: - TableView Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getTheatreData.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if getTheatreData.isEmpty{
            tableView.EmptyMessage(message: "No Theatre results found")
        }else{
            tableView.restore()
        }
        return   !getTheatreData.isEmpty ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellDefaultHeight: CGFloat = 140.0
        let screenDefaultHeight: CGFloat = view.frame.size.height
        let factor: CGFloat = cellDefaultHeight / screenDefaultHeight
        return factor * UIScreen.main.bounds.size.height
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = movieTable.dequeueReusableCell(withIdentifier: "TheatreCell", for: indexPath) as! CellTheatreDetailsTableViewCell
        //cell.layer.borderColor = UIColor.white.cgColor
        if !getTheatreData.isEmpty{
            let getData = self.getTheatreData[indexPath.row]
            cell.moviesName.text = getData["name"] as? String ?? ""
            cell.languageLabel.text = getData["movie_language"] as? String ?? ""
            if let getTime = getData["times"] as? [[String: Any]]{
                cell.updateTimeSection(getTimeData: getTime)
            }
            self.loadImageFromCell(imgStr: String(format: "%@%@", TAMBaseURL.baseURL, (getData["movie_posterimage"] as? String)!), imgViewNam: cell.movieImage)
                    }
        return cell
    }
    
    //MARK: - Calendar Kit delegate
    func dayViewDidSelectEventView(_ eventView: EventView) {
        
        //  print("Event has been selected: \(eventView)")
    }
    
    func dayViewDidLongPressEventView(_ eventView: EventView) {
        // print("Event has been selected:")
    }
    func dayViewDidLongPressTimelineAtHour(_ hour: Int) {
        // print("Event has been selected:")
    }
    
    func dayView(dayView: DayView, willMoveTo date: Date) {
        
        // print("Event has been selected:")
    }
    
    func dayView(dayView: DayView, didMoveTo date: Date) {
        //  print("DayView = \(dayView) did move to: \(date)")
        callTheatre(getDate: date)
    }
}

