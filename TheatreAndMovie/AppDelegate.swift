//
//  AppDelegate.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 07/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
import FacebookLogin
import IQKeyboardManagerSwift
import SwiftSpinner
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        //handle keyboard
        IQKeyboardManager.sharedManager().enable = true
        GIDSignIn.sharedInstance().clientID =  TAMKeys.googleKey
        //  GIDSignIn.sharedInstance().delegate = self
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        UITabBar.appearance().selectionIndicatorImage = getImageLine(color: UIColor.red, size: CGSize(width:(self.window?.frame.size.width)! / 8, height: 50), lineSize: CGSize(width: (self.window?.frame.size.width)! / 8, height: 3))
        // Thread.sleep(forTimeInterval: 3.0)
        // FirebaseApp.configure()
        UINavigationBar.appearance().tintColor = .red
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0.0, 0.0), for: .default)
        changeLoginVC()
        
        return true
    }
    func changeLoginVC(){
        if TAMUser.currentUser?.id != 0 &&  TAMUser.currentUser?.id != nil {  //TAMUser.isUserLoggedIn{
            let mainVC: MainTabBarViewController = UIStoryboard(storyboard: .main).instantiateViewController()
            window?.rootViewController = mainVC
        }else{
            let mainVC: LoginViewController = UIStoryboard(storyboard: .main).instantiateViewController()
            let navigationController = UINavigationController.init(rootViewController: mainVC)
            window?.rootViewController = navigationController
        }
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        let googleDidHandle = GIDSignIn.sharedInstance().handle(url,sourceApplication: sourceApplication,
                                                                annotation: annotation)
        let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(application,open: url,sourceApplication: sourceApplication,annotation: annotation)
        return googleDidHandle || facebookDidHandle
    }
    func getImageLine(color: UIColor, size: CGSize, lineSize: CGSize) -> UIImage{
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        let rectLine = CGRect(x: 0, y: size.height - lineSize.height, width: lineSize.width, height: lineSize.height)
        UIGraphicsBeginImageContext(size)
        UIColor.clear.setFill()
        UIRectFill(rect)
        color.setFill()
        UIRectFill(rectLine)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        return image
    }
   }
