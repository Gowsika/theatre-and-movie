//
//  ReviewViewController.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 28/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ReviewViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate {
    var getReviewText = [[String: Any]]()
    var placeholderLabel : UILabel!
    var getMovieId: String!
    var getPageNo: Int = 0
    var getUserID: Int!
    @IBOutlet weak var reviewWriteTextView: UITextView!
    @IBOutlet weak var reviewTableView: UITableView!
    @IBOutlet weak var maxCharLabel: UILabel!
          //MARK: - Load Methods
    override func viewDidLoad() {
        reviewTableView.removeEmptyCell()
       super.viewDidLoad()
        print(getMovieId)
        if let getID = getMovieId{
            getMovieId = getID
        }
        reviewWriteTextView.delegate = self
        reviewTableView.delegate = self
        reviewTableView.dataSource = self
        reviewWriteTextView.layer.borderWidth = 0.3
        reviewWriteTextView.textColor = UIColor.lightGray
        reviewTableView.rowHeight = UITableViewAutomaticDimension
        reviewTableView.estimatedRowHeight = 80
        reviewWriteTextView.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        createPlaceHolder(currentTextview: reviewWriteTextView)
        textViewDidChange(reviewWriteTextView)
       //  self.reviewTableView.setNeedsLayout()
       // self.reviewTableView.layoutIfNeeded()
       self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
       // reviewWriteTextView.becomeFirstResponder()
       //  addReviews()
        getReviews()
        
    }
func createPlaceHolder(currentTextview: UITextView){
    placeholderLabel = UILabel()
    placeholderLabel.text = "Write review about the movies"
   // placeholderLabel.font = UIFont.italicSystemFont(ofSize: (textView.font?.pointSize)!)
    placeholderLabel.sizeToFit()
    currentTextview.addSubview(placeholderLabel)
    placeholderLabel.frame.origin = CGPoint(x: 5, y: (currentTextview.font?.pointSize)! / 2)
    placeholderLabel.textColor = UIColor.lightGray
    placeholderLabel.isHidden = !currentTextview.text.isEmpty
    }
   @objc func doneButtonClicked() {
 if TAMUser.currentUser?.id == nil{
        SHOWAlert.setinfo(title: "CinemaTime", subTitle: "Login to Proceed")
    }
    if validate(textView: reviewWriteTextView) {
        // do something
        reviewWriteTextView.resignFirstResponder()
        addReviews(currentText: reviewWriteTextView.text)
    } else {
        // do something else
    }
    
    }
    func validate(textView: UITextView) -> Bool {
        guard let text = textView.text,
            !text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty else {
                return false
        }
        return true
    }
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !textView.text.isEmpty
        let totalCount: Int  = 500
        if textView.text.isEmpty{
             maxCharLabel.text = String(format:"Max %d characters allowed.",totalCount)
        }else{
             maxCharLabel.text = String(format:"%d characters remaining",totalCount - textView.text.count)
        }
        textView.textColor = !textView.text.isEmpty ? .black : .lightGray
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            doneButtonClicked()
            return false
        }
        return textView.text.count + (text.count - range.length) <= 500
    }
override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
       }
    // MARK: - Class Methods
    func addReviews(currentText: String){
        let params = ["movie_id": getMovieId, "user_id": TAMUser.currentUser?.id ?? Int(), "reviews" : currentText] as [String: Any]
        NetworkAdapter.methodPOST(.reviews(model: params), successCallback: { (response) in
            do{
                let getJSON: Any = try JSONSerialization.jsonObject(with: response.data, options: [])
                print(getJSON)
                let restArr = getJSON as! [String: Any]
                if restArr["Status"] as? String == "Success"{
                    
               //     let datum = restArr["Results"] as! [Any]
                    SHOWAlert.getSuccessAlert(title: "CinemaTime", subTitle:"Your Reviews are added Successfully", backgroundColor: .red)
                    self.reviewWriteTextView.text = ""
                    self.reviewWriteTextView.resignFirstResponder()
                    self.textViewDidChange(self.reviewWriteTextView)
                    self.getReviews()
                }
                
                 //                print(data)
//                if  !data.isEmpty{
//                if self.getReviewText.isEmpty{self.getReviewText.removeAll()}
//                self.getReviewText.append(data)
//                print(self.getReviewText)
//                                }
            }catch{
                print("getJSON")
            }
        }, error: { (error) in
            print("error", error.localizedDescription)
        }, failure: {(error) in
            print("error", error.localizedDescription)
        })
    }
    func getReviews(){
        let params = ["movie_id": getMovieId,"page" : getPageNo] as [String : Any]  
           NetworkAdapter.methodPOST(.gettingReview(model: params), successCallback: { (response) in
            do{
                let getJSON: Any = try JSONSerialization.jsonObject(with: response.data, options: [])
                let resultsArr = getJSON as! [String: Any]
                print(resultsArr)
                self.getReviewText.removeAll()
                if resultsArr["Status"] as? String == "Failed"{
               // let data1 = resultsArr["Results"] as! [Any]
               // SHOWAlert.setinfo(title: "", subTitle: (data1[0] as? String)!)
                 //   return
                }else{
                    let datas = resultsArr["Results"] as! [String: Any]
                    if !datas.isEmpty{
                        self.getReviewText = datas["data"] as! [[String : Any]]
                         self.getReviewText.reverse()
            }
                }
                    DispatchQueue.main.async {
                        self.reviewTableView.reloadData()
                    }
                    }catch{
                print("getJSON")
            }
        }, error: { (error) in
            print("error", error.localizedDescription)
        }, failure: {(error) in
            print("error", error.localizedDescription)
        })
    }
    
    //MARK: - TableView Delegate Methods
 func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return getReviewText.count
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if getReviewText.isEmpty{
            tableView.EmptyMessage(message: "No Reviews found")
        }else{
            tableView.restore()
        }
        return   !getReviewText.isEmpty ? 1 : 0
    }
    
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = reviewTableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! CellReviewTableViewCell
//             cell.reviewShownLabel.text = gets[indexPath.row]
//             cell.nameLabel.text = use[indexPath.row]
    cell.reviewShownLabel?.numberOfLines = 0
    cell.reviewShownLabel?.lineBreakMode = .byWordWrapping
    if !getReviewText.isEmpty{
        let getReviewDetails  = getReviewText[indexPath.row]
        cell.reviewShownLabel.text = nullToNil(value: getReviewDetails["reviews"] as AnyObject) as? String
        
        cell.daysAgoLabel.text = getPostedDate(getDate: (getReviewDetails["created_at"] as?  String)!)
        if let getUserDet = getReviewDetails["user_details"] as? [String: Any]{
            cell.nameLabel.text = nullToNil(value: String(format:"%@",getUserDet["user_fname"] as! CVarArg) as AnyObject) as? String // getUserDet["user_lname"] as! CVarArg
        }
    }
//    let readmoreFont = UIFont(name: "Helvetica-Oblique", size: 11.0)
//    let readmoreFontColor = UIColor.blue
//    let lengthForString: NSInteger = (cell.reviewShownLabel.text?.count)!
    
//    if (lengthForString >= 10){
//        cell.reviewShownLabel.addTrailing(with: "... ", moreText: "Readmore", moreTextFont: readmoreFont!, moreTextColor: readmoreFontColor)
//    }
              return cell
    }
 }
   func getPostedDate(getDate: String) -> String{
    //2018-08-07 11:06:15
    let dateformat  = DateFormatter()
    dateformat.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateformat.timeZone = TimeZone(abbreviation: "UTC")
    
    if let getdatefromString = dateformat.date(from: getDate){
        print(getdatefromString.timeAgoSinceDate(getSourceDate: getdatefromString, numericDates: true))
        return getdatefromString.timeAgoSinceDate(getSourceDate: getdatefromString, numericDates: true)
    }
    return ""
}

//      cell.reviewShownLabel.text = txt
//      reviewWriteTextView.text = ""
