//
//  ShowTimesViewController.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 27/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class ShowTimesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, UIPickerViewDelegate,UIPickerViewDataSource {
  var img = ["vox","vox","vox","vox", "vox", "vox", "vox", "vox", "vox", "vox", "vox", "vox", "vox", "vox", "vox"]
   var showTimeData = [[String: Any]]()
    var getMovieShowTime = [[String: Any]]()
    var getAudienceType = [[String: Any]]()
    var getMovieId: String!
    var movieId: Int!
     var selectedTimeStr: String!
    var setCurrentdate : String = ""
    var getprevMovieDetails: [String: Any]!
    var getAudienceVale = [String]()
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var timePickerView: UIPickerView!
    @IBOutlet weak var datePickerOutlet: UIDatePicker!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeOutlet: UIButton!
    @IBOutlet weak var dateOutlet: UIButton!
    @IBOutlet weak var showDetailTabel: UITableView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var movieTypeLabel: UILabel!
    
@IBOutlet weak var bottomView: UIView!
    
    //MARK: - Load Methods
    override func viewDidLoad() {
        self.showDetailTabel.removeEmptyCell()
        super.viewDidLoad()
        setBottomLine()
        print(getMovieId)
         dateLabel.text = getCurrentDate(getDate: Date())
        print(getprevMovieDetails)
        timePickerView.isHidden = true
      showDetailTabel.showsVerticalScrollIndicator = false
        datePickerOutlet.datePickerMode = .date
        datePickerOutlet.isHidden = true
         bottomView.isHidden = true
        callMovie(getDate: Date())
       getShowTime()
         }
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        }
    override func viewWillAppear(_ animated: Bool) {
        getMovieRunning()
             }
    func setBottomLine(){
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x: 0.0, y: 65, width: bgView.frame.size.width + 50, height: 1.0)
       bottomBorder.backgroundColor = UIColor(white: 0.8, alpha: 1.0).cgColor
        bgView.layer.addSublayer(bottomBorder)
    }
    // MARK: - getCurrentDate
    
    func callMovie(getDate: Date){
        setCurrentdate = getCurrentDate(getDate: getDate)
    }
    
    //MARK: - Class Methods
   
    
    //MARK: - Load ShowTime
    @objc func getShowTime(){
        let parameters = ["movie_id": getMovieId]
        NetworkAdapter.methodPOST(.showtime(model: parameters), successCallback: { (response) in
            do{
                let getJSON: Any = try JSONSerialization.jsonObject(with: response.data, options: [])
                let resultsArr = getJSON as! [String: Any]
                let data = resultsArr["Results"] as! [[String: Any]]
                if  !data.isEmpty{
                    if self.getMovieShowTime.isEmpty{self.getMovieShowTime.removeAll()}
                    self.getMovieShowTime.append(contentsOf: data)
                    print(self.getMovieShowTime)
                    self.timeLabel.text! = self.getMovieShowTime[0]["showtimes"] as? String ?? ""
                    print(self.getMovieShowTime[0]["showtimes"] as? String ?? "")
                    DispatchQueue.main.async {
                        self.timePickerView.reloadAllComponents()
                    }
                }
            }catch{
                print("getJSON")
            }
        }, error: { (error) in
            print("error", error.localizedDescription)
        }, failure: {(error) in
            print("error", error.localizedDescription)
        })
    }
    func getMovieParams() -> [String: Any]{
        if let getTemp = getprevMovieDetails, !getTemp.isEmpty{
            print(["movie_id": getTemp["id"] as? Int ?? 1, "date": dateLabel.text!, "time": "0", "audienceType" :  getAudienceType(type: movieTypeLabel.text!)])
            print(selectedTimeStr)
            
            if selectedTimeStr != nil{
                return["movie_id": getTemp["id"] as? Int ?? 1, "date": dateLabel.text!, "time": selectedTimeStr, "audienceType" : getAudienceType(type: movieTypeLabel.text!)]
            }else{
                return["movie_id": getTemp["id"] as? Int ?? 1, "date": dateLabel.text!, "time": "0", "audienceType" : getAudienceType(type: movieTypeLabel.text!)]}
            }else{
            return [:]
        }
    }
    //MARK: - movie Running In Theatres
         func getMovieRunning(){
          NetworkAdapter.methodPOST(.movieRun(model: getMovieParams()), successCallback: { (response) in
        do{
                let getJSON: Any = try JSONSerialization.jsonObject(with: response.data, options: [])
                let resultArray = getJSON as! [String : Any]
            if resultArray["Status"] as? String == "Failed"{
                 self.showDetailTabel.EmptyMessage(message: "No Theatres Found")
                    return
                }
                let data = resultArray["Results"] as! [String: Any]
                  print(data)
                if !data.isEmpty{
                    if self.showTimeData.isEmpty{self.showTimeData.removeAll()}
                    if let getTemp  = data["theatres"] as? [[String: Any]], !getTemp.isEmpty{
                        for getTheatre in getTemp{
                            print(getTheatre)
                             self.showTimeData.append(getTheatre)
                            print(self.showTimeData)
                            self.getAudienceType.append(getTheatre)
                        }
                    }
                    DispatchQueue.main.async {
                        self.showDetailTabel.reloadData()
                    }
                }
                }catch{
                    print("getJSON")
            }
        }, error: { (error) in
            print("error", error.localizedDescription)
        }, failure: {(error) in
            print("error", error.localizedDescription)
        })
             }
    
         //MARK: - GetAudienceType
    func getAudience(){
        NetworkAdapter.methodPOST(.getAudience(), successCallback: { (response) in
            do{
                let getJSON = try JSONSerialization.jsonObject(with: response.data, options: []) as! [String : Any]
                if  let resultArray = getJSON["Results"] as? [[String: Any]]{
                    print(resultArray)
                    if !resultArray.isEmpty{
                        for getAudienceData in resultArray{
                            print(getAudienceData)
                            let temp = getAudienceData["audience"] as! String
                            self.getAudienceVale.append(temp)
                            print(self.getAudienceVale)
                        }
                    }
                }
            }catch{
                print("getJSON")
            }
        }, error: { (error) in
            print("error", error.localizedDescription)
        }, failure: {(error) in
            print("error", error.localizedDescription)
        })
    }
    func getAudienceType(type: String) -> String{
        if type == "Male"{
            return "1"
        }else if type == "Family"{
            return "2"
        }else {
            return "0"
        }
    }
    
     // MARK: - Action Methods
    
    @IBAction func movieTypeAction(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Choose Movie Type", message: "", preferredStyle:  .actionSheet)
        alertController.addAction(UIAlertAction(title: "All", style: .default, handler: {(alert: UIAlertAction!) in
            self.movieTypeLabel.text = "All"
            self.showTimeData = []
            self.getMovieRunning()
            
        }))
        alertController.addAction(UIAlertAction(title: "Male", style: .default, handler: {(alert: UIAlertAction!) in
            self.movieTypeLabel.text = "Male"
            self.showTimeData = []
            self.getMovieRunning()
            
        }))
        alertController.addAction(UIAlertAction(title: "Family", style: .default, handler: {(alert: UIAlertAction!) in //self.getAudience()
            self.movieTypeLabel.text = "Family"
            self.showTimeData = []
            self.getMovieRunning()
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(action) -> Void in }))
        self.present(alertController, animated: true, completion:{})
     }
    @IBAction func dateAction(_ sender: Any) {
       if datePickerOutlet.isHidden == true  {
        datePickerOutlet.isHidden = false
        bottomView.isHidden = false
            timePickerView.isHidden = true
            showDetailTabel.isHidden = true
            datePickerOutlet.datePickerMode = .date
        }else {
            datePickerOutlet.isHidden = true
           bottomView.isHidden = true
        showDetailTabel.isHidden = false
             //timePickerView.isHidden = false
            }
    }
@IBAction func datepickerAction(_ sender: UIDatePicker) {
    //  let str = "7/22/2018"
        let calendars: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        var components: DateComponents = DateComponents()
        components.calendar = calendars
        components.day = 0
       let minDate = Calendar.current.date(byAdding: components, to: Date())
        var components1: DateComponents = DateComponents()
        components1.calendar = calendars
       components1.day = 10
       let maxDate = Calendar.current.date(byAdding: components1, to: Date())
        self.datePickerOutlet.minimumDate = minDate
        self.datePickerOutlet.maximumDate = maxDate
         let dateFormatter1 = DateFormatter()
      datePickerOutlet.datePickerMode = .date
      dateFormatter1.dateFormat = "yyyy-MM-dd"
       //dateFormatter1.dateStyle = .medium
        dateLabel.text = dateFormatter1.string(from: sender.date)
    }
      func timeChanged() {
        let dateFormatr = DateFormatter()
        dateFormatr.dateFormat = "h:mm a"
        let strDate = dateFormatr.string(from: Date())
        timeLabel.text = strDate
            }
   @IBAction func timeAction(_ sender: UIButton) {
    //     timePickerView.isHidden = !timePickerView.isHidden
    if timePickerView.isHidden == true  {
        timePickerView.isHidden = false
        bottomView.isHidden = false
        datePickerOutlet.isHidden = true
        showDetailTabel.isHidden = true
      }else {
        timePickerView.isHidden = true
        bottomView.isHidden = true
        showDetailTabel.isHidden = false
    }
             }
    @IBAction func cancelAction(_ sender: UIButton) {
        datePickerOutlet.isHidden = true
        showDetailTabel.isHidden = false
        bottomView.isHidden = true
        timePickerView.isHidden = true
    }
    @IBAction func doneAction(_ sender: UIButton) {
        self.showTimeData = []
        datePickerOutlet.isHidden = true
        showSpinner()
        getMovieRunning()
        showDetailTabel.isHidden = false
        bottomView.isHidden = true
        timePickerView.isHidden = true
      }
    //MARK: - Tableview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return showTimeData.count
    }
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = showDetailTabel.dequeueReusableCell(withIdentifier: "ShowTimeCell", for: indexPath) as! CellShowTimeTableViewCell
     if !showTimeData.isEmpty && !getAudienceType.isEmpty{
        let getOBJ = showTimeData[indexPath.row]
        print(getOBJ)
          cell.theatreNameLabel.text = getOBJ["name"] as? String ?? ""
        cell.cityNameLabel.text = getOBJ["city"] as? String ?? ""
       // print(getOBJ[indexPath.row]["showtimes"] as? [[String: Any]])
        if let getTime = getOBJ["showtimes"] as? [[String: Any]]{
            cell.updateShowTime(getTimeData: getTime)
            }
          }else if !getAudienceVale.isEmpty{
        
    }
        cell.theatreImage.image = UIImage(named: self.img[indexPath.row])
                 return cell
    }
         //MARK: - PickerView Delegate Method
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
       return 1
    }
func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return getMovieShowTime.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
      //  print(getMovieShowTime[row]["showtimes"] as? String ?? "")
    if !getMovieShowTime.isEmpty{
            let getData = getMovieShowTime[row]["showtimes"] as? String ?? ""
            return getData
        }
        return ""
            }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        timeLabel.text =   getMovieShowTime[row]["showtimes"] as? String ?? ""
        selectedTimeStr = getMovieShowTime[row]["showtimes"] as? String ?? ""
             }
}
