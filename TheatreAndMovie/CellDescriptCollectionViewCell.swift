//
//  CellDescriptCollectionViewCell.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 27/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit

class CellDescriptCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var characterLabel: UILabel!
    @IBOutlet weak var actorsImages: UIImageView!
    @IBOutlet weak var castCrewLabel: UILabel!
    @IBOutlet weak var portraitLabel: UILabel!
}
