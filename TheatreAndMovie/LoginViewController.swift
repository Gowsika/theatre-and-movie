//
//  LoginViewController.swift
//  TheatreAndMovie
//
//  Created by Gowsika on 07/06/18.
//  Copyright © 2018 com.dci. All rights reserved.
//

import UIKit
import GoogleSignIn
class LoginViewController: UIViewController,GIDSignInDelegate,GIDSignInUIDelegate {
@IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        getPaddingView()
        let navigationBar = navigationController?.navigationBar
        navigationBar?.tintColor = UIColor.red
        passwordTextField.textAlignment = .center
        userNameTextField.textAlignment = .center
        passwordTextField.layer.borderWidth = 1
        userNameTextField.layer.borderWidth = 1
       userNameTextField.layer.cornerRadius = 16
        userNameTextField.layer.masksToBounds = true
        passwordTextField.layer.cornerRadius = 16
        passwordTextField.layer.masksToBounds = true
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
       
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tap)
        
}
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    @objc func hideKeyboard(){
        view.endEditing(true)
    }
    func getPaddingView(){
        let enView = UIImageView(frame: CGRect(x: 10, y: 0, width: 25, height: 19))
        enView.image = UIImage(named: "user_icon-2")
        enView.contentMode = .scaleAspectFit
        let testView = UIView(frame: CGRect(x: 10, y: 0, width: 25, height: 19))
        testView.addSubview(enView)
        userNameTextField.leftView?.frame = enView.frame
        userNameTextField.leftView = testView
        userNameTextField.leftViewMode = .always
        
        let paddingView = UIImageView(frame: CGRect(x: 10, y: 0, width: 25, height: 19))
        paddingView.image = UIImage(named: "pass_icon-2")
        paddingView.contentMode = .scaleAspectFit
        let testingView = UIView(frame: CGRect(x: 10, y: 0, width: 25, height: 19))
        testingView.addSubview(paddingView)
        passwordTextField.leftView?.frame = paddingView.frame
        passwordTextField.leftView = testingView
        passwordTextField.leftViewMode = .always
    }
    @IBAction func forgotPasswordAction(_ sender: UIButton) {
        let forgot = storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
        self.navigationController?.pushViewController(forgot, animated: true)
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        let nav = storyboard?.instantiateViewController(withIdentifier: "MainTabBarViewController") as! MainTabBarViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
    @IBAction func faceBookAction(_ sender: UIButton) {
        FacebookSDK.FaceBook()
    }
    @IBAction func googleAction(_ sender: UIButton) {
        GoogleSDK.GooglePlus()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func skipAction(_ sender: UIButton) {
        let nav = storyboard?.instantiateViewController(withIdentifier: "MovieDetailViewController") as! MovieDetailViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
    @IBAction func signUpAction(_ sender: UIButton) {
       self.navigationController?.popToRootViewController(animated: true)
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil{
            print(error ?? "google error")
            return
        }
    }
    
}
